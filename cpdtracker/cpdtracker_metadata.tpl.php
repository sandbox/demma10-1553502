<?php

/**
 * @file
 * Default theme implementation for CPD custom items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) profile type label.
 * - $url: The URL to view the current profile.
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - profile-{TYPE}
 * - $name: The name of this CPD custom item.
 * - $status: The status (active or inactive) of the item.
 * - $username: The username that this CPD custom item belongs to.
 * - $e_date: The date the event or activity occured.
 * - $credit: The amount of credit this CPD item provides. Does not include
 *   the metric unit
 * - $unit: The unit of measurement for the credit.
 * - $all_cpds: An array of all cpd elements attached to this item. In
 *   the form of array[set_id][cpd_id][element_id] => element_name. There will
 *   be sepearate templates for rendering each component.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="cpd-item-metadata"<?php print $content_attributes; ?>>
    <p><strong><?php print t('Name:') ?></strong> <?php print $name ?><br />
       <strong><?php print t('Status:') ?></strong> <?php print $status ?><br />
       <?php if(isset($username)) { ?>
         <strong><?php print t('Usernmae:') ?></strong> <?php print $username ?><br />
       <?php } ?>
       <strong><?php print t('Date of Activity:')?></strong> <?php print $e_date ?><br />
       <strong><?php print t('Credit:') ?></strong> <?php print $credit ?> <em><?php print $unit ?></em>
    </p>
  </div>

  <?php /* Add any user fields if we have them.*/ ?>
  <?php if(!empty($fields)) { ?>
    <h2>User data</h2>
  <?php
    foreach ($fields as $field) {
      print render($field);
    }
  }
  ?>

  <?php /* Add the header for the cpd standards. The sets, cpds and elements
           each have their own template so are not included in here.*/ ?>
  <?php if($has_cpds == TRUE) { ?>
      <h2><?php print t("Continuing Professional Development"); ?></h2>
  <?php } ?>
</div>
