<?php

/**
 * @file
 * The install file for the CPD tracker module. This file sets up the database
 * table which stores the CPD tracking entities.
 */

/**
* Implementation of hook_schema
* Define the database schemas for the CPD Tracker item entity.
*/
function cpdtracker_schema() {
  $schema['cpdtracker'] = array(
    'description' => 'The base table for the CPD tracker item entities.',
    'fields' => array(
      'ctr_id' => array(
        'description' => 'The primary identifier for this CPD tracker record.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'The status of this cpd tracker record. Possible values are active and inactive.',
        'type' => 'varchar',
        'size' => 30,
        'not null' => TRUE,
        'default' => 'active',
      ),
      'uid' => array(
        'description' => 'The id of the user this item belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'obj_id' => array(
        'description' => 'The identifier of the object with cpds attached.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'obj_type' => array(
        'description' => 'The type of object with the CPD attached, eg node user.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'elements_override' => array(
        'description' => 'A list of elements that this tracker item will use instead of the elements from the original cpd item.',
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
      ),
      'value_override' => array(
        'description' => 'Allows the credit value of the original item to be overriden.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('ctr_id'),
  );

  return $schema;
}

/**
* Implementation of hook_install
* Install the cpdtracker module by creating the database tables.
* Even though this function is empty it must exist to prompt the automatic
* calling of hook_schema.
*/
function cpdtracker_install() {
  return;
}

/**
* Implementation of hook_uninstall
* Uninstall the cpdtracker module by removing the database tables.
* This function must exist to prompt the automatic uninstallation of the
* database tables.
*/
function cpdtracker_uninstall() {
  return;
}

/**
* Add status column to the cpdtracker table.
*/
function cpdtracker_update_7100() {
  $spec = array(
    'description' => 'The status of this cpd tracker record. Possible values are active and inactive.',
    'type' => 'varchar',
    'length' => 30,
    'not null' => TRUE,
    'default' => 'active',
  );

  db_add_field( 'cpdtracker', 'status', $spec);

  //Set the status for all existing records in the database.
  db_update('cpdtracker')
    ->fields(array('status' => 'active'))
    ->execute();

}
