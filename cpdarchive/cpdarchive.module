<?php

/**
 * @file
 * The module file for the CPD Archive module. This module allows CPD items and tracker
 * records to be removed from the regular CPD module tables and stored in special archive
 * tables. Archived CPD items no longer appear in the user's tracker and thus do not clutter
 * up the interface. Also the module provides the option to archive CPD item and tracker
 * entities when the original entity they are linked to (ie nodes, users) is deleted. Archived
 * items can then be deleted from the archive tables without affecting the users.
 */

/**
 * Implementation of hook_help
 */
function cpdarchive_help($path, $arg) {
  $output = '';
   switch ($path) {
    case "admin/help#cpdarchive":
      $output = '<p>' . t("The CPD Custom module allows users with appropriate permissions to move CPD items and tracker items out of their module's own database tables and into the archive tables. This allows for two things; the regular tables remain uncluttered and CPD information can be preserved when the original entity they link to is deleted.") . '</p>';
      break;
  }
  return $output;
}

/**
 * Implementation of hook_permissions
 */
function cpdarchive_permission() {
  return array(
    'administer CPD archive items' => array(
      'title' => t('Administer CPD Archive'),
      'description' => t('Allow administration of the CPD archive module.'),
    ),
    'archive CPD and tracker items' => array(
      'title' => t('Archive CPD items and tracker items'),
      'description' => t('Allow users to archive CPD and tracker items.'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 * We have two very similar entities here.
 */
function cpdarchive_entity_info() {

  $return = array(
    'cpdarchive_item' => array(
      'label' => t('CPD Archive Item Entity'),
      'entity class' => 'Entity',
      'controller class' => 'EntityAPIController',
      'base table' => 'cpdarchive_item',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'cai_id',
        'label' => 'name',
      ),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'cpdarchive_admin_access',
      'module' => 'cpdarchive',
      //'metadata controller class' => 'EntityMetadataController',
    ),
    'cpdarchive_tracker' => array(
      'label' => t('CPD Archive Tracker Item Entity'),
      'entity class' => 'Entity',
      'controller class' => 'EntityAPIController',
      'base table' => 'cpdarchive_tracker',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'cat_id',
        'label' => 'cat_id',
      ),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'cpdarchive_admin_access',
      'module' => 'cpdarchive',
      //'metadata controller class' => 'EntityMetadataController',
    ),
  );

  return $return;
}

/**
 * Implements hook_theme().
 */
/*function cpdarchive_theme() {
  return array(
    'cpdnode_admin_view' => array(
      'render element' => 'elements',
      'template' => 'cpdnode_metadata',
    ),
    'cpdnode_set_view' => array(
      'render element' => 'elements',
      'template' => 'cpd-set',
    ),
    'cpdnode_standard_view' => array(
      'render element' => 'page_elements',
      'template' => 'cpd-standard',
    ),
    'cpdnode_element_view' => array(
      'render element' => 'elements',
      'template' => 'cpd-element-list',
    ),
    'cpdnode_nodemetadata_view' => array(
      'render element' => 'elements',
      'template' => 'cpdnode_nodemetadata',
    ),
  );
}*/

/**
 * Administration interface access callback. Returns TRUE if the user has
 * permission to access the CPD Archive administration interface.
 */
function cpdarchive_admin_access($op, $type = NULL, $account = NULL) {
  return user_access('administer CPD archive items', $account);
}

/**
 * Implementation of hook_menu.
 * Add all of the CPD node pages to the Drupal menu system.
 * CPDs can be attached to nodes in two ways, via the admin pages
 * or on the node edit page.
 */

function cpdarchive_menu() {
  $items = array();

  //Set the URLs for the archive admin pages.
  $items['admin/structure/cpdsuite/cpdarchive_admin'] = array(
    'title' => 'Administer CPD Archive Items',
    'description' => 'Administer the CPD and tracker items which have been archived.',
    'file' => 'cpdarchive.admin.inc',
    'page callback' => 'cpdarchive_admin_cpd_page',
    'access arguments' => array('administer CPD archive items'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/structure/cpdsuite/cpdarchive_admin/archived'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'Archived',
    'weight' => 0,
  );

  $items['admin/structure/cpdsuite/cpdarchive_admin/live'] = array(
    'title' => 'Live',
    'description' => 'Show all live CPD items.',

    'file' => 'cpdarchive.admin.inc',
    'page callback' => 'cpdarchive_admin_live_cpd_page',
    'access arguments' => array('administer CPD archive items'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,

  );

  //This is the primary callback function used to archive CPD items.
  $items['admin/cpdsuite/cpdarchive_admin/cpd/%/%/archive'] = array(
    'title' => 'Archive',
    'description' => 'Archive this item',
    'file' => 'cpdarchive.admin.inc',
    'page callback' => 'cpdarchive_archive_item_page',
    'page arguments' => array(4, 5),
    'access arguments' => array('administer CPD archive items'),
    'type' => MENU_CALLBACK,
  );

  //Set the url for the settings page.
  $items['admin/config/system/cpdarchive_admin'] = array(
    'title' => 'Administer CPD Archive Settings',
    'description' => 'Administer settings which handle the archiving of CPDs.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cpdarchive_admin_settings'),
    'access arguments' => array('administer CPD archive items'),
    'type' => MENU_NORMAL_ITEM,
  );


  //The view, edit and delete callbacks for archived items.
  $items['admin/structure/cpdsuite/cpdarchive_admin/%'] = array(
    'title' => 'View',
    'description' => 'View the selected CPD archive item.',
    'file' => 'cpdarchive.admin.inc',
    'page callback' => 'cpdarchive_admin_view_cpd_page',
    'page arguments' => array(4),
    'access arguments' => array('administer CPD archive items'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/structure/cpdsuite/cpdarchive_admin/%/view'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'View',
    'page arguments' => array(4),
    'weight' => 0,
  );
  $items['admin/structure/cpdsuite/cpdarchive_admin/%/edit'] = array(
    'title' => 'Edit',
    'description' => 'Edit the selected CPD archive item.',
    'file' => 'cpdarchive.admin.inc',
    'page callback' => 'cpdarchive_admin_edit_cpd_page',
    'page arguments' => array(4),
    'access arguments' => array('administer CPD archive items'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/structure/cpdsuite/cpdarchive_admin/%/delete'] = array(
    'title' => 'Delete',
    'description' => 'Delete the selected CPD archive item.',
    'file' => 'cpdarchive.admin.inc',
    'page callback' => 'cpdarchive_admin_delete_cpd_confirm_page',
    'page arguments' => array(4),
    'access arguments' => array('administer CPD archive items'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );

  //Add the URL for the allowed node types page.
  /*$items['admin/structure/cpdnode_admin/settypes'] = array(
    'title' => 'Set the Allowed Node Types',
    'description' => 'Set which node types may have CPDs attached to them.',
    'file' => 'cpdnode.admin.inc',
    'page callback' => 'cpdnode_admin_cpd_settypes',
    'access arguments' => array('administer CPD node items'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/structure/cpdnode_admin/cpd/add'] = array(
    'title' => 'Add new CPD node item',
    'description' => 'Add a new CPD item for a node.',
    'file' => 'cpdnode.admin.inc',
    'page callback' => 'cpdnode_admin_add_cpd_page',
    'access arguments' => array('administer CPD node items'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/structure/cpdnode_admin/cpd/%'] = array(
    'title' => 'View',
    'description' => 'View the selected CPD node item.',
    'file' => 'cpdnode.admin.inc',
    'page callback' => 'cpdnode_admin_view_cpd_page',
    'page arguments' => array(4),
    'access arguments' => array('administer CPD node items'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/structure/cpdnode_admin/cpd/%/view'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'View',
    'page arguments' => array(4),
    'weight' => 0,
  );
  $items['admin/structure/cpdnode_admin/cpd/%/edit'] = array(
    'title' => 'Edit',
    'description' => 'Edit the selected CPD node item.',
    'file' => 'cpdnode.admin.inc',
    'page callback' => 'cpdnode_admin_edit_cpd_page',
    'page arguments' => array(4),
    'access arguments' => array('administer CPD node items'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/structure/cpdnode_admin/cpd/%/delete'] = array(
    'title' => 'Delete',
    'description' => 'Delete the selected CPD node item.',
    'file' => 'cpdnode.admin.inc',
    'page callback' => 'cpdnode_admin_delete_cpd_confirm_page',
    'page arguments' => array(4),
    'access arguments' => array('administer CPD node items'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );

  //Item to migrate the D6 tables to the new D7 table.
    $items['admin/structure/cpdnode_admin_migrate'] = array(
    'title' => 'Migrate CPD Node Items',
    'description' => 'Migrate the node CPD items from D6 to D7.',
    'file' => 'cpdnode_migrate.inc',
    'page callback' => 'cpdnode_migrate_cpd_page',
    'access arguments' => array('administer CPD node items'),
    'type' => MENU_NORMAL_ITEM,
  );*/

  return $items;
}

/*These functions perform the actual archiving and retrieval of items from the archive
*tables.*/

/**
 * This function handles the archiving process of items. Special care must be taken for
 * archiving of tracker items.
 * @param $type
 *   the type of CPD item we wish to archive.
 * @param $id
 *   The id of the item we wish to archive.
 */
function cpdarchive_perform_archive($type, $id) {

  //Retrive the data needed for archiving. The type and id should be enough to identify.
  //a unque cpd entity, therefore only one module should return data to module_invoke_all.
  $data = module_invoke_all('cpdarch_grab_archiving_data', $type, $id);

  //Retrive the delete flag for this type of item. This is done via another hook invocation.
  //Again under normal operation only one value should return. As module_invoke_all returns an
  //array, we need to extract the flag from the array (simple enough, it should be the first
  //value).
  $del_arr = module_invoke_all('cpdarch_grab_delete_flag', $type);
  $del_flag = FALSE;
  if(!empty($del_arr)) {
    $del_flag = $del_arr[0];
  }

  //Retrieve any tracking data associated with the given item. This hook is a general hook
  //not just limited to the archive module. This hook should only be invoked if the tracker
  //module is enabled. Also there is another delete flag, this time for the tracker module.
  //Note also that we can archive tracker items. If the item type is a tracker item then we
  //ignore this bit of code (remember tracker items cannot track themselves).
  $tracker_data = array();
  $t_del_flag = TRUE;
  if($type != 'tracker' && module_exists('cpdtracker')) {
    $tracker_data = module_invoke('cpdtracker', 'cpdarch_grab_tracking_data', $type, $id);
    $tdel_arr = module_invoke('cpdtracker', 'cpdarch_grab_delete_flag', 'tracker');
    $t_del_flag = $tdel_arr[0];
  }

  //Initialise the arch_id variable. We'll return this.
  $arch_id = 0;

  //Now we split this function into two branches. The first branch handles the case that
  //the item we wish to archive is a tracking item. The second branch handles the case that
  //the item we wish to archive is a normal item.
  if($type == 'tracker') {
    //When archiving tracking data a copy of the item being tracked must also be archived
    //and the archived tracking item must point to that copy.

    //First check that the item being tracked is not already in the archive table.
    $arch_orig = _cpdarchive_is_item_archived($data->orig_type, $data->orig_id);

    //If arch_orig is FALSE then the item is not in the archive table. We
    //first need to archive a copy of the item being tracked.
    if($oarch_orig == FALSE) {
      //Yes call the data retrieval hook again. This time to get the item to be tracked
      //data.
      $o_data = module_invoke_all('cpdarch_grab_archiving_data', $data->orig_type, $data->orig_id);
      $data['aobj_id'] = _cpdarchive_archive_item($o_data);
    }
    else {
      //The item to be tracked is already in the archive table. Therefore arch_orig will be
      //id of the item to be tracked in the archive table.
      $data['aobj_id'] = $arch_orig;
    }

    //Archive the tracker item.
    $arch_id = _cpdarchive_archive_tracker($data);
  }
  else {
    //The item to be archived is a regular item. Just archive that item.
    $arch_id = _cpdarchive_archive_item($data);

    //Loop through any tracking data, archiving it in the tracker archive table.
    //Only do this if the tracker module is enabled.
    if(module_exists('cpdtracker') && !empty($tracker_data)) {
      $arch_tr_id = array();
      foreach ($tracker_data as $tdata) {
        //Set the archived object id in tdata to $arch_id before
        //archiving the tracking data.
        $tdata['aobj_id'] = $arch_id;
        $atr_id = _cpdarchive_archive_tracker($tdata);
        $arch_tr_id[] = $atr_id;
      }
    }
  }

  //After archiving decide what to do with the original objects.

  //The delete flag is a boolean with only two values, TRUE or FALSE. If set to TRUE then
  //the original item must be deleted from the database after archiving, along with all of the
  //tracker data. If FALSE then only a copy of the item and its tracking data is archived,
  //leaving the original intact.

  if($del_flag == TRUE) {
    //Invoke the delete hook for the given item.
    $ret = module_invoke_all('cpdh_item_delete', $type, $id);

    //If the tracker module is enabled and the item has tracking data set to be deleted
    //then delete that data.
    if(module_exists('cpdtracker') && $t_del_flag == TRUE) {
      foreach ($tracker_data as $tdata) {
        $ret = module_invoke('cpdtracker', 'cpdh_item_delete', $tdata->tr_id);
      }
    }
  }

  //Reached here return success.
  return $arch_id;
}

/**
* This function archives the main item in its database table. It does not handle
* the archiving of tracker data (tracker data is a special case).
* @param $data
*  The item data which is to be archived. This is an associative array of the form
*  expected by the cpdarchive entity. It is up to the calling function to make sure that
*  the array is of the form required by cpdarchive.
* @return
*  returns the cpdarchive_id of the newly archived object. This is needed when archiving
*  tracker data.
*/
function _cpdarchive_archive_item($data) {
  //TODO - Add error checking here.

  //Create a new archive entity from all of the data in the data array.
  $ent = entity_create('cpdarchive_item', $data);

  //Save that entity into the cpdarchive_item table.
  entity_save('cpdarchive_item', $ent);

  $id = entity_id('cpdarchive_item', $ent);

  //Now that the entity is saved it should have an entity id. Get that id and
  //return it to the calling function.

  return $id;
}

/**
* This function archives a tracker item in the tracker archive table.
* @param $data
*  The item data which is to be archived. This is an associative array of the form
*  expected by the cpdarchive entity. It is up to the calling function to make sure that
*  the array is of the form required by cpdarchive.
* @return
*  returns the cpdarchive_id of the newly archived object. This is needed when archiving
*  tracker data.
*/
function _cpdarchive_archive_tracker($data) {
  //TODO - Add error checking here.

  //Create a new archive entity from all of the data in the data array.
  $ent = entity_create('cpdarchive_tracker', $data);

  //Save that entity into the cpdarchive_item table.
  entity_save('cpdarchive_tracker', $ent);

  //Now that the entity is saved it should have an entity id. Get that id and
  //return it to the calling function.

  return $ent->cat_id;
}

/**
* This function restores an item from the archive. In this function
* the differences between normal archived items and tracker items
* is handled by the helper functions.
* @param $type
*   The cpd item type we wish to retrieve.
* @param $id
*   The id of the archived item we wish to retrieve.
* @return
*   Returns TRUE on success and FALSE on error.
*/
function cpdarchive_perform_restore($type, $id) {
  //Use the value of $type to determine which database
  //table the entity needs to be loaded from.
  $table = '';
  if($type == 'tracker') {
    $table = 'cpdarchive_tracker';
  }
  else {
    $table = 'cpdarchive_item';
  }

  //Retrieve the archived entity.
  $ent = entity_load($table, $id);

  //Grab the delete flag. This will determine how the restore is
  //to be carried out. If no module returns a delete flag than
  //be safe and assume FALSE.
  $del_arr = module_invoke_all('cpdarch_grab_delete_flag', $ent->orig_type);
  $del_flag = FALSE;
  if (!empty($del_arr)) {
    $del_flag = $del_arr[0];
  }

  //Split the function in two depending on the value of the
  //delete flag.

  if ($del_flag == FALSE) {
    //Look up the original entity.
    $ent_orig_arr = module_invoke_all('cpdarch_grab_live_item', $type, $ent->orig_id);

    //If module_inovoke_all returns empty then it can be assumed
    //that the original entity is no longer in the live table.

    if (!empty($ent_orig_arr)) {
    //Recreate the live entity from the archived version.
    $new_ent_id = _cpdarchive_new_from_archive($type, $ent);

    //If the new entity failed to be created then just return.
    if ($new_ent_id == NULL) {
      return ;
    }

    //Hadle all archived tracker items which point to this entity.
      if ($type != 'tracker') {
        //Update the archived tracker items to now point to the restored
        //entity.
        $retval = _cpdarchive_update_archive_tracker_data($ent->cai_id, $new_ent_id);
      }
    }
    else {
    $ent_orig = $ent_orig_arr[0];
    //Overwrite the entity in the live table with the one from the
    //archive table.
    _cpdarchive_overwrite_from_archive($ent_orig, $ent);
    }
  }
  elseif ($del_flag == TRUE) {
    //The archived item does not exist in the the live table. Therefore
    //create a new entity from the archive.
    $new_ent_id = _cpdarchive_new_from_archive($type, $ent);

    //If the new entity failed to be created then just return.
    if ($new_ent_id == NULL) {
      return ;
    }

    //Hadle all archived tracker items which point to this entity.
    if ($type != 'tracker') {
      //Update the archived tracker items to now point to the restored
      //entity.
      $retval = _cpdarchive_update_archive_tracker_data($ent->cai_id, $new_ent_id);
    }
  }

  return;

}

/**
 * The first of two functions to restore an item from the archive tables.
 * This function assumes the original item is still in the live table
 * and will overwrite the values of that entity with the values from
 * the archived entity.
 * @param $ent_live
 *   The current live entity.
 * @param $ent_arch
 *   The archived version of the entity.
 * @return
 *   At present the function doesn't return anything. In the future
 *   may change this to return an error flag.
 */
function _cpdarchive_overwrite_from_archive($ent_live, $ent_arch) {
  //Overwrite the generic members of the live entity with the saved
  //versions from the archived entity.
  $ent_live->name = $ent_arch->name;
  $ent_live->e_date = $ent_arch->e_date;
  $ent_live->value = $ent_arch->value;
  $ent_live->elements = $ent_arch->elements;

  //Loop through the item_data from the archived entity, using
  //the values to overwrite the unique entity data.
  foreach ($ent_arch->item_data as $key => $value) {
    $ent_live->$key = $value;
  }

  return;
}

/**
 * The second of two functions to restore an item from the archive tables.
 * This function assumes the original item was deleted from the live
 * table (either during archiving or by an administrator). It will
 * recreate the live entity from the entity saved in the archive.
 * @param $type
 *   The type of the cpd item entity. This will be used in a hook
 *   get the original entity table.
 * @param $ent_arch
 *   The entity from the archives.
 * @return $live_id
 *   Returns the entity id of the new live entity if it could be
 *   created and NULL on error.
 */
function _cpdarchive_new_from_archive($type, $ent_arch) {
  //Use the type to get the cpd item entity table.
  $tables = module_invoke_all('cpdarch_get_entity_type', $type);

  //If $tables is empty then we couldn't determine the type of
  //the live entity so return NULL.
  if (!empty($tables)) {
    return NULL;
  }
  //Grab teh entity type from the returned array.
  $ent_type = $tables[0];

  //Set up the new entity data using the archived data.
  $data = array();

  //Build the new entity. First add the generic elements common to
  //all cpd item entities.
  $data['name'] = $ent_arch->name;
  $data['e_date'] = $ent_arch->e_date;
  $data['value'] = $ent_arch->value;
  $data['elements'] = $ent_arch->elements;

  //Loop through the item_data array adding the unique entity-specific
  //data.
  foreach ($ent_arch->item_data as $key => $value) {
    $data[$key] = $value;
  }

  //Create the new entity and save it.
  $new_live = entity_create($ent_type, $data);
  entity_save($ent_type, $new_live);

  //Get the id of the entity and return it.

  return entity_id($ent_type, $new_live);
}

/* Function to update any tracker items in the archive table pointing
 * to a newly restored cpd item. All we need to do is update the original
 * item id to the new value.
 * @param $cai_id
 *   The id of the archived cpd item which was just restored. This is
 *   used to find any archived tracker objects in the database.
 * @param $new_id
 *   The id of the newly created live cpd item entity.
 * @return
 *   Returns TRUE on success and FALSE on error.
 */

function _cpdarchive_update_archive_tracker_data($cai_id, $new_id) {
  //Try with a database update query. For such a simple operation
  //I may be able to get better performance by bypassing the entity
  //API.

  $num_updated = db_update('cpdarchive_tracker')
    ->fields(array(
      'obj_id' => $new_id,
    ))
    ->condition('aobj', $cai_id, '=')
    ->execute();

  return TRUE;
}

/******************************************************/
/* Utility Functions. */
/******************************************************/

/**
 * Function to load all of the CPD archive item entities without
 * element information. This
 * function uses a custom database query instead of entity_load
 * because we do not need all of the available fields.
 * @return
 *  Returns an array of objects containing all of the CPD item metadata.
 *  The item data is returned serialised.
 */
function cpdarchive_get_all_items_metadata() {
  //Set the return array.
  $ret_val = array();

  //Set up the query and placeholder tokens array.
  $query = 'SELECT cai_id, orig_id, name, type, a_date, e_date, value, item_data FROM {cpdarchive_item}';
  $result = db_query($query);

  //Believe it or not, fetchAllAssoc returns an array of OBJECTS keyed
  //by the field provided in it's argument. NOT an array of arrays keyed
  //by the field provided in it's argument.
  $ret_val = $result->fetchAllAssoc('cai_id');

  //If there is an error or no records then fetchAll will return FALSE.
  //Send an empty array back to the caller in this case.
  if ($ret_val == FALSE) {
    return array();
  }

  return $ret_val;
}

/**
 * Function to load all of the CPD archive item entities. This function
 * includes the loading of the element information.
 * @return
 *  Returns an array of CPD node item entities.
 */
function cpdarchive_get_all_items() {
  //Use the entity_load function to load all entities of our profile type.
  $conditions = array();
  $reset = FALSE;

  $ret_val = entity_load('cpdarchive_item', FALSE, $conditions, $reset);

  return $ret_val;
}

/**
* Function which given an item_id returns the name of that item. For such a
* simple request we don't bother loading the entity, just do a static query.
* @param $item_id
*   The id of the entity we wish to look up. 
* @return
*   Returns the name if the element exists or FALSE otherwise.
*/

function _cpdarchive_get_item_name($item_id) {
  $query = 'SELECT name FROM {cpdarchive_item} WHERE cai_id = :id';

  $result = db_query_range($query, 0, 1, array(':id' => $item_id));

  //If result returns FALSE or the name is empty then we return
  //FALSE.
  if ($result == FALSE || count($result) <= 0) {
    return FALSE;
  }

  //Fetch the name.
  $res_obj = $result->fetchObject();

  return $res_obj->name;
}

/**
 * Function to create a list of all active users on the site. I find it hard
 * to believe that there is no API function to get a list of all users
 * on the site.
 * @return
 *   Returns an array of the form uid => user_name. Returns NULL on error.
 */

function _cpdarchive_get_all_users() {
  //Get all active users. As user is a core module we don't need it
  //as a dependency.
  $query = 'SELECT uid, name FROM {users} WHERE status = 1';
  $result = db_query($query);

  //If there are no results for the query then $result will be
  //empty. Return the empty array.
  if (count($result) <= 0) {
    return array();
  }
  //If something went wrong then $result will equal FALSE.
  //This is an error so we return NULL
  if ($result == FALSE) {
    return NULL;
  }

  //Fetch the results using fetchAllKeyed. The first column is uid and the
  //second column is username.
  $all_users = $result->fetchAllKeyed(0, 1);

  return $all_users;
}

