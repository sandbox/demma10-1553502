<?php
/**
 * @file
 * The installation file of CPD Archive.
 */

/**
* Define the database schemas for the cpdarchive entity. I will need to use two
* entity tables for archiving, one is for holding archives of CPD items and other is
* holding tracker entities. The reason for two entities is because the tracker and item
* entities are quite different from each other.
*/
function cpdarchive_schema() {

  $schema['cpdarchive_item'] = array(
  'description' => 'The table for storing archived CPD items.',
    'fields' => array(
       'cai_id' => array(
            'description' => 'The primary identifier for this entity.',
            'type' => 'serial',
            'unsigned' => TRUE,
            'not null' => TRUE,
       ),
       'orig_id' => array(
        'description' => 'The original id of the CPD item before it was archived.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The name of the CPD item.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
         'description' => 'The type of original object, eg CPD node item, CPD custom item.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'a_date' => array(
        'description' => 'The date that this CPD item was archived. Stored as a UNIX timestamp.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
       ),
      'e_date' => array(
        'description' => t('The date that this CPD was issued, in most cases this is the date that the event which provides the CPD occured. Stored as a UNIX timestamp.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
       ),
      'value' => array(
        'description' => 'The value or credit for this item or activity',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'elements' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of elements of CPD standards attached to this entity.',
      ),
      'item_data' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of extra information that the orignial entity contained, eg. node id, user id. The item_data is a black box for extra data that the original entities need to carry around with them.',
      ),
    ),
  'primary key' => array('cai_id'),
);

  $schema['cpdarchive_tracker'] = array(
    'description' => 'The base table for the archived CPD tracker entities.',
    'fields' => array(
      'cat_id' => array(
        'description' => 'The primary identifier for this archived CPD tracker record.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'orig_id' => array(
        'description' => 'The original id of tracker record before it was archived.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The id of the user this item belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'aobj_id' => array(
        'description' => 'The identifier of the object with cpds attached. This id points to an object in the cpdarchive_item table.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'a_date' => array(
        'description' => 'The date that this CPD tracker record was archived. Stored as a UNIX timestamp.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
       ),
      'obj_id' => array(
        'description' => 'The identifier of the object with cpds attached. This id points to a non-archived object in the original table.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'obj_type' => array(
        'description' => 'The type of object with the CPD attached, eg node user.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'elements_override' => array(
        'description' => 'A list of elements that this tracker item will use instead of the elements from the original cpd item.',
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
      ),
      'value_override' => array(
        'description' => 'Allows the credit value of the original item to be overriden.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('cat_id'),
  );

 return $schema;

}

/**
 * Implementation of hook_install.
 * Intialise the archive on delete variable.
 */

function cpdarchive_install() {
  variable_set('cpdarchive_archive_on_delete', FALSE);
  return;
}

/**
 * Implementation of hook_uninstall.
 * Delete the archive on delete variable.
 */
function cpdarchive_uninstall() {
  variable_del('cpdarchive_archive_on_delete');
  return;
}

