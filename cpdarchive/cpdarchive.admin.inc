<?php

/**
 * @file
 * This file contains the CPD Archive Administration functions.
 */

/*********************************************************/
/* Settings Page. */
/*********************************************************/

/**
 * These functions control the settings page for the archive
 * module.
 *
 * At present there is only one setting, archive_on_delete. When this value
 * is set to TRUE than when an entity which is linked to a CPD item gets deleted
 * than the CPD items and tracker records automatically get moved into the archive
 * table. When archive_on_delete is set to FALSE then when an entity is
 * deleted all CPD data linked to that entity is also deleted.
 * @return
 *    Returns system_settings_form($form). For the D7 version this is a render array.
 */
function cpdarchive_admin_settings($form, &$form_state) {
  $form = array();

  //Create the archive on delete options array.
  $options = array();
  $options[TRUE] = t('Archive CPD information.');
  $options[FALSE] = t('Delete CPD information.');

  //Create the admin settings form.
  $form['cpdarchive_delete_action'] = array(
    '#type' => 'radios',
    '#title' => t('When content or users get deleted, what should be done with any linked CPD information'),
    '#options' => $options,
    '#default_value' => variable_get('cpdarchive_archive_on_delete', FALSE),
    '#description' => t("Select how to handle CPD information attached to content or users about to be deleted."),
  );

  return system_settings_form($form);
}

/*********************************************************/
/* Administration Pages. */
/*********************************************************/

/**
* Code to create the CPD archive item administration page.
* This page is just one big table which lists all the archived CPD items
* along with options to edit and delete each item.
*
* Due to the way archiving works there is no option to create a new archive item.
*/
function cpdarchive_admin_cpd_page() {

  drupal_set_title(t('CPD Archive Items Administration'), PASS_THROUGH);

  //Create the render array for the output.
  $page_content = array();

  //Add the introductory text.
  $page_content['intro'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('Manage archived CPD items for the whole site from this page.') . '</p>',
  );

    //Add a heading for the archived items table.
  $page_content['item_header'] = array(
    '#type' => 'markup',
    '#markup' => '<h3>' . t('Archived CPD Items') . '</h3>',
  );

  //Create the common table headers.
  $table_headers = array();
  $table_headers[] = t('Item Name');
  $table_headers[] = t('Original Item type');
  $table_headers[] = t('Date of Archiving');
  $table_headers[] = t('Original Date of Item');
  $table_headers[] = t('Value');
  $table_headers[] = t('Other Item Information');
  //Add the operation links.
  $table_headers[] = array('data' => t('Operations'), 'colspan' => '3');

  //Obtain all of the cpd items both name user and id.
  $all_items = cpdarchive_get_all_items_metadata();

  //Test to see what I come back with.
  //print_r($all_items);
  //exit();
  //If there are no items display just the add item link and return.
  if (empty($all_items)) {
    return $page_content;
  }

  //Reached here then we have items to display. We will display the
  //items in a table.

  //Create the normal rows for the table.
  $rows = array();

  //Now using a foreach we build the rows.
  foreach ($all_items as $item_id => $item) {
    $view_item_link = 'admin/structure/cpdsuite/cpdarchive_admin/' . $item_id . '/view';
    $edit_item_link = 'admin/structure/cpdsuite/cpdarchive_admin/' . $item_id . '/edit';
    $delete_item_link = 'admin/structure/cpdsuite/cpdarchive_admin/' . $item_id . '/delete';
    $item_name = check_plain($item->name);
    $item_type = check_plain($item->type);
    $archive_date = date('d-m-Y', $item->a_date);
    $event_date = date('d-m-Y', $item->e_date);
    $value = check_plain($item->value);
    $item_data = unserialize($item->item_data);

    //Create a HTML string of the unserialised item data.
    //A very dodgy way to do this.
    $item_data_string = '';
    if(!empty($item_data)) {
      foreach ($item_data as $key => $val) {
        $item_data_string .= '<em>' . check_plain($key) . ':</em> ' . check_plain($val) . '<br />';
      }
    }

    //Create the row.
    $rows[] = array(
      $item_name,
      $item_type,
      $archive_date,
      $event_date,
      $value,
      $item_data_string,
      l('View', $view_item_link),  
      l('Edit', $edit_item_link), 
      l('Delete', $delete_item_link),
    );
  }

  //Create the table.
  $table_vars = array('header' => $table_headers, 'rows' => $rows);
  $table = theme('table', $table_vars);

  //Render the table and add it to page content.
  $page_content['cpd_table'] = array(
    '#type' => 'markup',
    '#markup' => $table,
  );

  //Add a heading for the archived tracker items table.
  /*$page_content['item_header'] = array(
    '#type' => 'markup',
    '#markup' => '<h3>' . t('Archived Tracker Items') . '</h3>',
  );*/

  //TODO Place code for archived tracker items here.

  return $page_content;
}

/**
* Code to create the CPD archive item administration page.
* This page is just one big table which lists all the archived CPD items
* along with options to edit and delete each item.
*
* Due to the way archiving works there is no option to create a new archive item.
*/
function cpdarchive_admin_live_cpd_page() {

  drupal_set_title(t('CPD Archive Live Items Administration'), PASS_THROUGH);

  //Create the render array for the output.
  $page_content = array();

  //Add the create new CPD set link.
  $page_content['intro'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('Listed here are CPD items which can be archived. To archive an item click on the Archive link next to that item.') . '</p>',
  );

  //Create the common table headers.
  $table_headers = array();
  $table_headers[] = t('Item Name');
  $table_headers[] = t('Item type');
  $table_headers[] = t('Date of Item');
  $table_headers[] = t('Value');
  $table_headers[] = t('Other Item Information');
  //Add the operation links.
  $table_headers[] = array('data' => t('Operations'), 'colspan' => '1');

  //Obtain all of the cpd items which can be archived. This is achieved via
  //a hook.
  $all_items = module_invoke_all('cpdarchive_get_all_arc_items');

  //Test to see what I come back with.
  //print_r($all_items);
  //exit();
  //If there are no items display just the add item link and return.
  if (empty($all_items)) {
    return $page_content;
  }

  //Reached here then we have items to display. We will display the
  //items in a table.

  //Create the normal rows for the table.
  $rows = array();

  //Now using a foreach we build the rows.
  foreach ($all_items as $item_key => $item) {
    $item_type = check_plain($item->type);
    $item_id = $item->item_id;
    $archive_item_link = 'admin/cpdsuite/cpdarchive_admin/cpd/' . $item_type . '/' . $item_id . '/archive';
    $item_name = check_plain($item->name);
    $event_date = date('d-m-Y', $item->e_date);
    $value = check_plain($item->value);
    $item_data = $item->item_data;

    //Create a HTML string of the unserialised item data.
    //A very dodgy way to do this.
    $item_data_string = '';
    foreach ($item_data as $key => $val) {
      $item_data_string .= '<em>' . check_plain($key) . ':</em> ' . check_plain($val) . '<br />';
    }

    //Create the row.
    $rows[] = array(
      $item_name,
      $item_type,
      $event_date,
      $value,
      $item_data_string,
      l('Archive', $archive_item_link),
    );
  }

  /*//Create the table.
  $table_vars = array('header' => $table_headers, 'rows' => $rows);
  $table = theme('table', $table_vars);

  //Render the table and add it to page content.
  $page_content['cpd_table'] = array(
    '#type' => 'markup',
    '#markup' => $table,
  );*/

  //Create a render array which will be themed as a table with a pager.
  $page_content['pager_table'] = array(
    '#theme' => 'table', 
    '#header' => $table_headers, 
    '#rows' => $rows, 
    '#empty' => t('No users are tracking CPDs at this time.'),
  );

  //Attach the pager theme
  $page_content['pager_pager'] = array('#theme' => 'pager');

  return $page_content;
}


/*TODO: Move most of this code into a theme function and then into a
template file.*/

function cpdarchive_admin_view_cpd_page($item_id) {

  //Load the cpd custom item entity. We don't make use of the extra
  //parameters for entity_load as yet. The function entity_load returns
  //an array of entity objects. As we are only retrieving a single entity
  //we need to extract that object from the array.
  $item_arr = entity_load('cpdarchive_item', array($item_id));
  
  //If item_arr is empty then this entity id is invalid. Set a message
  //and return the empty array.
  if(empty($item_arr)) {
    drupal_set_message(t('Sorry, this CPD item does not exist.'), 'warning');
    return array();
  }

  //Extract the lone entity from the array item_arr.
  $item = $item_arr[$item_id];

  //Get the information about the item with given item id.
  $name = $item->name;
  $type = $item->type;
  $orig_id = $item->orig_id;
  $a_date = $item->a_date;
  $e_date = $item->e_date;
  $value = $item->value;
  $item_data = $item->item_data;

  //Grab all of the cpds and elements attached to this item.
  //Copied from cpd_node.

  $all_cpds = $item->elements;

  //If there are no cpds attached to this item then set the has_cpds
  //flag to FALSE.
  $has_cpds = TRUE;
  if(empty($all_cpds)) {
    $has_cpds = FALSE;
  }

  //Add the title.
  $title_string = t('CPD Archive Item - %name', array('%name' => $name));
  drupal_set_title($title_string, PASS_THROUGH);

  //Add the value for this item. Attach the metric suffix to the end.
  $metric_id = variable_get('cpdcore_global_metric', 1);
  $unit_name = cpdcore_get_metric_unit_name($metric_id);

  //Create the render array for the page content.
  $page_content = array();

  //Add the basic information.
  /*$page_content['item_metadata'] = array(
    '#theme' => 'cpdarchive_admin_view',
    'name' => $name,
    'username' => $username,
    'e_date' => $e_date,
    'credit' => $value,
    'unit' => $unit_name,
    'has_cpds' => $has_cpds,
  );

  //Loop over all the cpds adding them onto the page.
  foreach ($all_cpds as $set_id => $cpd_items) {

    //Get the set name.
    $set_name = cpdcore_get_set_name($set_id);
    //Add the set name to the render array.
    $set_key = 'set_' . $set_id;
    $page_content[$set_key] = array(
      '#theme' => 'cpdarchive_set_view',
      'set_id' => $set_id,
      'name' => $set_name,
      'cpds' => array(),
    );

    //Loop over all the cpd items adding them to the page.
    foreach ($cpd_items as $cpd_id => $elements) {
      //Given the cpd_id obtain the standard/cpd name.
      $cpd_name = cpdcore_get_cpd_name($cpd_id);

      //Add the cpd to the page.
      $cpd_key = 'cpd_' . $cpd_id;
      $page_content[$set_key]['cpds'][$cpd_key] = array(
        '#theme' => 'cpdarchive_standard_view',
        'cpd_id' => $cpd_id,
        'name' => $cpd_name,
        'elements' => array(),
      );

      //Loop through each of the element ids adding them to the render
      //array.
      foreach ($elements as $key => $element_id) {
        $element_name = cpdcore_get_element_name($element_id);
        $element_key = 'element_' . $element_id;
        $page_content[$set_key]['cpds'][$cpd_key]['elements'][$element_key] = array(
          '#theme' => 'cpdarchive_element_view',
          'e_id' => $element_id,
          'name' => $element_name,
        );
      }
    }
  }

  //print_r($page_content);
  //exit();*/

  return $page_content;
}

function cpdarchive_admin_edit_cpd_page($item_id) {
  //Get the name of the CPD.

  $name = _cpdarchive_get_item_name($item_id);
  if ($name == FALSE) {
    drupal_set_message(t('Item does not exist in the database'), 'warning');
    //Redirect to the admin page.
    //$form_state['#redirect'] = 'cpdcore_admin_cpd_page';
    return array();
  }

  //Create the page title.
  $titlestring = t('Edit CPD Custom Item') . ' ' . check_plain($name);
  drupal_set_title($titlestring, PASS_THROUGH);

  //Define the page content array.
  $page_content = array();

  //Add the form to the page.
  $page_content['form'] = array(
    'type' => 'markup',
    'markup' => drupal_get_form('cpdarchive_admin_edit_item_form', $item_id),
  );

  return $page_content;
}

/*Function to create the edit item form. This form is the same as the
add form except that we add values already stored in the database
as default values. The unused node argument is now mandatory otherwise
the function signature is wrong and we cannot access the item_id arguemnt.*/

function cpdarchive_admin_edit_item_form($node, $form_state, $item_id) {

  //Load the entity.
  $item_arr = entity_load('cpdarchive_item', array($item_id));
  
  //If item_arr is empty then this entity id is invalid. Set a message
  //and return the empty array.
  if(empty($item_arr)) {
    drupal_set_message(t('Sorry, this CPD item does not exist.'), 'warning');
    return array();
  }

  //Extract the lone entity from the array item_arr.
  $item = $item_arr[$item_id];

  //Get the information about the item with given item id.
  $name = $item->name;
  $type = $item->type;
  $a_date = cpdcore_timestamp2array($item->a_date);
  $e_date = cpdcore_timestamp2array($item->e_date);
  $value = $item->value;

  //Grab all of the cpds and elements attached to this item.
  //Copied from cpd_node.

  $entity_cpds = $item->elements;

  //The array which holds the form.
  $form['item_name'] = array(
  '#type' => 'textfield',
  '#title' => t('Item Name'),
  '#default_value' => check_plain($name),
  '#required' => TRUE,
  '#description' => "Please enter the name of your item.",
  '#size' => 40,
  '#maxlength' => 255,
  );

  $form['a_date'] = array(
  '#type' => 'date',
  '#title' => t('Date of Achiving'),
  '#default_value' => $a_date,
  '#required' => TRUE,
  '#description' => "",
  );

  $form['e_date'] = array(
  '#type' => 'date',
  '#title' => t('Date of Activity'),
  '#default_value' => $e_date,
  '#required' => TRUE,
  '#description' => "",
  );

  //Make a fieldset for the extra item data if any exists.
  if(!empty($item->item_data)) {
    //Make a fieldset for the cpd code.
    $form['item_data'] = array(
      '#type' => 'fieldset',
      '#title' => t('Other Item Data'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    //Display all of the extra item data as a set of textfields.
    $data = $item->item_data;
    foreach ($data as $key => $val) {
      //Append the fieldset name to the key.
      $key2 = 'item_data_' . $key;
      //Create the element.
      $form['item_data'][$key2] = array(
        '#type' => 'textfield',
        '#title' => check_plain($key),
        '#default_value' => $val,
        '#description' => "",
      );
    }
  }

  //Add a field for the credit value.
  $metric_id = variable_get('cpdcore_global_metric', 0);
  $suffix = cpdcore_get_metric_unit_name($metric_id);
  $form['item_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Credit'),
    '#default_value' => check_plain($value),
    '#required' => TRUE,
    '#description' => "Enter in the credit for this item.",
    '#size' => 20,
    '#maxlength' => 255,
    '#field_suffix' => $suffix,
  );

  //Add the sets, cpds and elements. Copied straight from cpd_node.

  $complete_sets = cpdcore_get_all_cpds_wsets();

  if (empty($complete_sets)) {
    return;
  }

  //Make a fieldset for the cpd code.
  $form['cpd_attached_sets'] = array(
        '#type' => 'fieldset',
        '#title' => t('Key Performance Indicators'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,

  );

  //Get to displaying the forms. First separate into the sets.
  foreach ($complete_sets as $set_id => $all_cpds) {
    //From the set name create a new non-collapsable fieldset.
    $set_name_uc = cpdcore_get_set_name($set_id); 
    $form['cpd_attached_sets'][$set_id] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($set_name_uc),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    //Get the unit name of the metric for this set.
    $metric_id = cpdcore_get_metric_from_set_id($set_id);
    $suffix = cpdcore_get_metric_unit_name($metric_id);

    foreach ($all_cpds as $cpd_id => $cpd_item) {
      //Again use the cpd_id.
      $cpd_name = cpdcore_get_cpd_name($cpd_id);
      //Merge the set id and cpd id to get a unique key.
      //Needed for form_state as it doesn''t seem to be
      //preserving the array sturcutre.
      $cpd_index = $set_id . "__" . $cpd_id;

      //Now to add the elements.
      $element_index = $cpd_index . "__elements";

      //Set the title of the elements control to be the name of the cpd
      //standard.
      $element_title = $cpd_name;

      $all_elements = cpdcore_get_elements($cpd_id);
      if (empty($all_elements)) {
        continue;
      }
      $default_checked = array();
      if(!empty($entity_cpds[$set_id][$cpd_id])) {
        $default_checked = array_keys($entity_cpds[$set_id][$cpd_id]);
      }
      $form['cpd_attached_sets'][$set_id][$element_index] = array(
        '#type' => 'checkboxes',
        '#title' => check_plain($element_title),
        '#default_value' => $default_checked,
        '#options' => $all_elements,
      );
    }
  }

  //Add the item_id as a value. Will be needed in the submit function.
  $form['item_id'] = array(
    '#type' => 'value',
    '#value' => $item_id,
  );

  //Also add the type value.
  $form['item_type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );

  $form['edit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  //Manually add the default submit function.
  //$form['#submit'][] = 'cpd_direct_admin_add_cpd_item_form_submit';

  return $form;

}

/*The submission handler for the admin custom item edit form. This form
is the same as the add item form we just include the item id as part of
the item structure. Again draw inspiration from the cpd_node module.*/

function cpdarchive_admin_edit_item_form_submit($form, &$form_state) {
  //If the Add button was clicked then we add this item.
  if ($form_state['clicked_button']['#value'] == t('Save')) {
    //Create empty arrays for the cpd and elements data.
    $cpd_array = array();
    $elements = array();

    $elements_complete = array();
    //Grab the cpd information. and place it in a usable form.
    $complete_sets = cpdcore_get_all_cpds_wsets();
    foreach ($complete_sets as $set_id => $all_cpds) {
      foreach ($all_cpds as $cpd_id => $cpd_item) {
        //Form API does not preserve the tree
        //structure for fieldsets. Thus we need to
        //look up the component index directly.
        //Fortunately we used the set_id and the
        //cpd_id to index the component so we can
        //easily recreate the index.
        $cpd_index = $set_id . "__" . $cpd_id;

        //Grab the elements for this cpd.
        $element_index = $cpd_index . "__elements";
        $element_keys = array();
        if (isset($form_state['values'][$element_index])) {
          $element_keys = $form_state['values'][$element_index];
        }
        //Loop through the keys, adding them to the
        //elements array if they are selected.
        foreach ($element_keys as $key) {
          if ($key > 0) {
        //Make an array with all the data ready to be inserted into
        //the database. I know the element key is repeated, but again
        //this is for simplicity. Plus I really have no meaningful value
        //to store here other than TRUE (ie the element was selected).
          $elements_complete[$set_id][$cpd_id][$key] = $key;
          }
        }
        
      }
    }

    //Get the item value.
    $value = $form_state['values']['item_value'];

    //Get the date information. A feature of mktime is that its
    //arguments are in American date order. Has caused numerous
    //errors. We store dates as UNIX timestamps.
    $e_date_raw = $form_state['values']['e_date'];
    $e_date = mktime(0, 0, 0, $e_date_raw['month'], $e_date_raw['day'], $e_date_raw['year']);

    //Do the same for the a_date variable.
    $a_date_raw = $form_state['values']['a_date'];
    $a_date = mktime(0, 0, 0, $a_date_raw['month'], $a_date_raw['day'], $a_date_raw['year']);

    //TODO: Place this index retrieval code into its own function.
    //Thanks to form_state['values'] not preserving the tree structure
    //I have to go search for the right keys manually.
    $full_keys = array_keys($form_state['values']);
    $item_data_keys = array();
    foreach ($full_keys as $k) {
      if(strpos($k, 'item_data_') !== FALSE) {
        $item_data_keys[] = $k;
      }
    }

    //Go through and add any extra item data if we have it.
    $item_data = array();
    if(!empty($item_data_keys)) {
      foreach ($item_data_keys as $keyr) {
        //Strip the front of the key off using str_replace.
        $key = str_replace('item_data_', '', $keyr);
        $item_data[$key] = $form_state['values'][$keyr];
      }
    }

    //Create the item structure
    $item = array(
      'cai_id' => $form_state['values']['item_id'],
      'type' => $form_state['values']['item_type'],
      'name' => $form_state['values']['item_name'],
      'e_date' => $e_date,
      'a_date' => $a_date,
      'value' => $value,
      'item_data' => $item_data,
      'elements' => $elements_complete,
    );

    //Create the custom cpd item entity and save it into the database.
    $custom_entity = entity_create('cpdarchive_item', $item);

    //To alert the save routine that we are updating an existing entity
    //unset the is_new variable.
    unset($custom_entity->is_new);

    //Save the new entity.
    $return_value = entity_save('cpdarchive_item', $custom_entity);

    //$return_value = _cpd_direct_save_item('update', $item);

    if ($return_value == FALSE) {
      drupal_set_message(t('An error occured. Changes not saved'), 'error');
      $form_state['redirect'] = 'admin/structure/cpdsuite/cpdarchive_admin/' . check_plain($form_state['values']['item_id']) . '/edit';
    }
    else {
      drupal_set_message(t('Item updated.'), 'status');
      $form_state['redirect'] = 'admin/structure/cpdsuite/cpdarchive_admin/cpd/' . check_plain($form_state['values']['item_id']) . '/view';
    }

  }

  return ;

}

/**
 * Menu callback to create the item delete confirmation page.
 */

function cpdarchive_admin_delete_cpd_confirm_page($item_id) {
  return drupal_get_form('cpdarchive_admin_delete_item_confirm', $item_id);
}

/**
 * Function to create the delete confirmation form. Like the edit form
 * builder I need to assign the not-very-useful node parameter to get at
 * my item_id argument.
 * @param $node
 *   A non-useful first argument
 * @param $form_state
 *   The current state of the form. Useful when resetting after an error.
 * @param $item_id
 *   The id of the item we wish to delete.
 * @return
 *   Returns a form array containing the delete confirmation form.
 */

function cpdarchive_admin_delete_item_confirm($node, $form_state, $item_id) {
  //Get the cpd name from the database, this will be used in the
  //confirmation message.

  $name = _cpdarchive_get_item_name($item_id);
  if ($name == FALSE) {
    drupal_set_message(t('Item does not exist in the database'), 'warning');
    //Redirect to the admin page.
    //$form_state['#redirect'] = 'cpdcore_admin_cpd_page';
  }

  //Need to send the element_id along with the form.
  $form['item_id'] = array(
    '#type' => 'value',
    '#value' => $item_id,
  );

  //Return the confirmation form.
  return confirm_form($form,
    t('Are you sure you want to delete the Archived Item %title?', array(
      '%title' => $name)),
    'admin/cpdsuite/cpdarchive_admin',
    t('Once deleted this item cannot be retrieved.'),
    t('Delete'),
    t('Cancel')
  );
  return ;
}

/*The submit handler for the admin delete item form.*/

function cpdarchive_admin_delete_item_confirm_submit($form, &$form_state) {
  //Create the item array. For deletes it only contains the one
  //item.
  $item = array('item_id' => $form_state['values']['item_id']);
  $item_id = $form_state['values']['item_id'];

  //Call the entity delete function.
  $return_value = entity_delete('cpdarchive_item', $item_id);

  //Unfortunately entity_delete only returns FALSE in this case so I cannot
  //determine whether the delete succeeded just form the return value.
  //Assuming success and continuing anyway.
  drupal_set_message(t('Item deleted'), 'status');
  $form_state['redirect'] = 'admin/structure/cpdsuite/cpdarchive_admin';

  return ;
}

/*************************************************/
/* Archiving and Un-archiving pages. */
/*************************************************/

/**
 * Page callback for archiving a live item. Returns back to the
 * main archiving page once archiving is successful.
 * @param $type
 *  The type of item we are archiving
 * @param $id
 *  The id of the item we are archiving.
 */

function cpdarchive_archive_item_page($type, $id) {
  //TODO: Add an 'Are you sure?' message, simmilar to how delete
  //forms work.
  //Call the archiving function.
  cpdarchive_perform_archive($type, $id);

  //Set the success message.
  $message = t('Item archived successfully.');
  drupal_set_message($message, 'status');

  //Redirect to the archived tab.
  drupal_goto('admin/structure/cpdsuite/cpdarchive_admin');
}

