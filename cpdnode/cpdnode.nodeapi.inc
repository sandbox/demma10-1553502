<?php

/**
* @file
* File containing all of the nodeapi hooks for this module.
* These hooks are placed in a separate file to keep the module file clean.
*/

/**
* Implementation of hook_node_validate
*/
function cpdnode_node_validate($node, $form, &$form_state)
{
  //A big thankyou to the Drupal 7 developers for including the $form and
  //$form_state variables into the function signature, you have just made
  //my validation code a lot more straightforward.

  //Check that the node is of a type which allows CPDs to be attached to it.
  //If it isn't than we end validation right here.

  $cpds_allowed = _cpdnode_is_type_valid($form_state['values']['type']);
  if($cpds_allowed == FALSE) {
    return;
  }


  //Get the CPD Node item value data out of the form_state array.
  //For some bizzare reason form_state does not preserve the fieldset
  //structure of the form.

  $metric_id = variable_get('cpd_core_global_metric', 1);
  $item_value = $form_state['values']['cpdnode_item_value'];

  $limit_type = cpd_core_get_metric_limit_type($metric_id);

  //If the limit type equals unlimited then we don't need to validate,
  //as a limit type of unlimited allows for any value whatsoever (regardless
  //of whether it makes sense or not). Just return.
  if($limit_type == 'unlimited') {
    return;
  }

  //Reached here than we have a limit with constraints. Get the limit array.
  $limit_array = cpd_core_get_metric_limit_array($metric_id);

  if($item_value != '') {
    //First do the range check. IF the limit type is range then
    //the value must lie between the first two elements of limit_array
    //(we ignore the rest of limit array). Assuming numeric data.
    if($limit_type == 'range' && ($cpd_value < $limit_array[0] || $cpd_value > $limit_array[1])) {
      $error_message = t('Value %item_value is not within the allowed range: %min_range - %max_range', array('%item_value' => $item_value, '%min_range' => $limit_array[0], '%max_range' => $limit_array[1]));
      form_set_error('item_value',$error_message);
    }

    //Now do the list check. The value entered must be in the
    //limit_array otherwise it is not valid. For the moment do
    //a case-sensetive no trim search. May want to make this
    //search case-insensetive or add the trim function (I do
    //know that some people when they are typing always add
    //an extra whitespace to the end of what they are typing).
    if($limit_type == 'list' && array_search($cpd_value, $limit_array) === FALSE) {
      $error_message = t('Value %item_value is not an allowed value for this CPD', array('%item_value' => $item_value));
      form_set_error('item_value', $error_message);
    }
  }

  return;
}

/**
* Implementation of hook_node_delete.
*/

function cpdnode_node_delete($node) {

  $nid = $node->nid;
  //Find the entity id associated with this node.
  $cne_id = _cpdnode_get_item_id($nid);
  //If the id is NULL than no entity is attached to this node. Return.
  if ($cne_id == NULL) {
    return;
  }
  //Delete the cpd node entity associated with this node.
  entity_delete('cpdnode', $cne_id);

  //If tracker is loaded then delete the tracker data for
  //this node.
  if(module_exists('cpdtracker')) {
    cpdtracker_object_item_delete($nid);
  }

}

/**
* Implementation of hook_node_insert.
* The code in this function is based on the code from the add form submit handler.
* @param $node
*   Should really be called $form_state as esentially this is all the data
*   that would be in $form_state of a form. Again for some reason fieldsets
*   are not preserved.
*/

function cpdnode_node_insert($node) {

  //At this point the node object has already been inserted into
  //the database, so it has a nid and a type.

  //Check that the node is of a type which allows CPDs to be attached to it.
  //If it isn't than we end validation right here.
  $cpds_allowed = _cpdnode_is_type_valid($node->type);
  if($cpds_allowed == FALSE) {
     return;
  }

  $nid = $node->nid;
  //Get the node title, the date and the value.
  $name = $node->title;
  $e_date = $node->cpdnode_e_date;
  $value = $node->cpdnode_item_value;

  //Add a flag variable. This flag is set when we have elements to set.
  $elements_flag = FALSE;

  $elements_complete = array();

  //Grab the cpd information. and place it in a usable form.
  $complete_sets = cpd_core_get_all_cpds_wsets();
  foreach ($complete_sets as $set_id => $all_cpds) {
    foreach ($all_cpds as $cpd_id => $cpd_item) {
      //Form API does not preserve the tree
      //structure for fieldsets. Thus we need to
      //look up the component index directly.
      //Fortunately we used the set_id and the
      //cpd_id to index the component so we can
      //easily recreate the index.

      //Grab the elements for this cpd.
      $element_index = $set_id . "__" . $cpd_id . "__cpdnode_elements";
      $element_keys = array();
      if (isset($node->$element_index)) {
        $element_keys = $node->$element_index;
      }
      //Loop through the keys, adding them to the
      //elements array if they are selected.
      foreach ($element_keys as $key) {
        if ($key > 0) {
          //If we reached in here than we have some elements which
          //were selected. Set the element flag.
          $element_flag = TRUE;
          //Make an array with all the data ready to be inserted into
          //the database. I know the element key is repeated, but again
          //this is for simplicity. Plus I really have no meaningful value
          //to store here other than TRUE (ie the element was selected).
          $elements_complete[$set_id][$cpd_id][$key] = $key;
        }
      }    
    }
  }

  //If the user entered in a credit value OR if the user selected
  //elements, create the new cpdnode entity and insert it into the
  //database. We don't check the e_date value as there is a default
  //value entered into that field (ie it will be very rare to NOT have
  //a value in that field).
  if($elements_flag == TRUE || $value != '') {
     $item = array(
       'name' => $name,
       'nid' => $nid,
       'value' => $value,
       'e_date' => $e_date,
       'elements' => $elements_complete,
     );
     $ent = entity_create('cpdnode', $item);
     entity_save('cpdnode', $ent);
  }
    
  return;
}

/**
* Implementation of hook_node_updaste.
* Because of the way entities are handled this function does insert, update
* and delete operations. Insert a new entity into the database if the
* editing of the node causes one to be created. Update an existing entity
* if changes are made. Finally delete the entity if one exists and all CPD
* node values are removed from the edit form.
* @param $node
*   The node object which is being updated. Like hook_node_insert this
*   parameter should really be named $form_state as it behaves like the
*   the form_state parameter in FormAPI.
*/

function cpdnode_node_update($node) {

  //At this point the node object has already been inserted into the database, so it
  //has a nid and a type.

  //Check that the node is of a type which allows CPDs to be attached to it.
  //If it isn't than we end validation right here.
  $cpds_allowed = _cpdnode_is_type_valid($node->type);
  if($cpds_allowed == FALSE) {
     return;
  }

  $nid = $node->nid;
  //Get the node title, the date and the value.
  $name = $node->title;
  $e_date_raw = $node->cpdnode_e_date;
  $e_date = mktime(0, 0, 0, $e_date_raw['month'], $e_date_raw['day'], $e_date_raw['year']);
  $value = $node->cpdnode_item_value;

  //Add a flag variable. This flag is set when we have elements to set.
  $elements_flag = FALSE;

  $elements_complete = array();

  //Grab the cpd information. and place it in a usable form.
  $complete_sets = cpd_core_get_all_cpds_wsets();
  foreach ($complete_sets as $set_id => $all_cpds) {
    foreach ($all_cpds as $cpd_id => $cpd_item) {
      //Form API does not preserve the tree
      //structure for fieldsets. Thus we need to
      //look up the component index directly.
      //Fortunately we used the set_id and the
      //cpd_id to index the component so we can
      //easily recreate the index.

      //Grab the elements for this cpd.
      $element_index = $set_id . "__" . $cpd_id . "__cpdnode_elements";
      $element_keys = array();
      if (isset($node->$element_index)) {
        $element_keys = $node->$element_index;
      }
      //Loop through the keys, adding them to the
      //elements array if they are selected.
      foreach ($element_keys as $key) {
        if ($key > 0) {
          //If we reached in here than we have some elements which
          //were selected. Set the element flag.
          $element_flag = TRUE;
          //Make an array with all the data ready to be inserted into
          //the database. I know the element key is repeated, but again
          //this is for simplicity. Plus I really have no meaningful value
          //to store here other than TRUE (ie the element was selected).
          $elements_complete[$set_id][$cpd_id][$key] = $key;
        }
      }    
    }
  }

  //If the user entered in a credit value OR if the user selected
  //elements, create the new cpdnode entity and insert it into the
  //database. We don't check the e_date value as there is a default
  //value entered into that field (ie it will be very rare to NOT have
  //a value in that field).
  if($elements_flag == TRUE || $value != '') {
     $item = array(
       'name' => $name,
       'nid' => $nid,
       'e_date' => $e_date,
       'value' => $value,
       'elements' => $elements_complete,
     );
     $ent = entity_create('cpdnode', $item);
     $cne_id = _cpdnode_get_item_id($node->nid);
     if($cne_id != NULL) {
       $item['cne_id'] = $cne_id;
       $ent = entity_create('cpdnode', $item);
       unset($ent->is_new);
     }
     entity_save('cpdnode', $ent);
  }

  //Handle the case for deletion of the entity object. If there is no
  //value set and no elements are set (again we ignore the date parameter)
  //then we assume that we we wish to delete the entity if it already exists.
  if($elements_flag == FALSE && $value == '') {
    $cne_id = _cpdnode_get_item_id($node->nid);
    if($cne_id != NULL) {
      entity_delete('cpdnode', $cne_id);
    }
  }
    
  return;
}

/**
* Implementation of hook_node_load.
* Care must be taken here. As unlike in D6 multiple nodes may be passed to
* this function. For performance reasons this function asks that you load
* all data from the database for all nodes in one query so that's what we'll
* do. We'll obtain all the nids for the nodes passed to the function,
* load only the data needed for display and then loop through each node
* attaching the corresponding cpdnode data to it.
* @param $nodes
*   An array of node keyed by id being loaded.
* @param $types
*   The types of all those nodes being loaded, again keyed by id.
*/

function cpdnode_node_load($nodes, $types)
{

  //Check that the node is of a type which allows CPDs to be attached to it.
  //If it isn't than we end validation right here.
  $allowed_types = variable_get('cpdnode_allowed_types');
  //Use the array intersect function to test whether any of the given types are
  //one of the allowed values.
  $intersect = array_intersect($types, $allowed_types);
  //If none of the nodes to be loaded are of a valid type to contain cpdnode entities
  //just return.
  if(empty($intersect)) {
     return;
  }

  //Obtain an array of nids. We know from the Drupal node documentation that $nodes is
  //an array keyed by nids.
  $nids = array_keys($nodes);

  //Get all of the cpd node data associated with the given nids.
  //This array is keyed by the nids not cne_ids (you'll see why further
  //down). Not all of the entity information is needed so we don't call
  //entity_load (may decide to change this in the future).

  //For a lot of nodes and a lot of entities THIS FUNCTION WILL BE SLOW.
  $cpdnodes = _cpdnode_get_all_items_keynid($nids);

  if($cpdnodes == NULL) {
    return;
  }

  //Loop through each item attaching it to its corresponding node.
  //Looks like I can add object members on the fly in PHP.
  foreach ($cpdnodes as $nid => $item) {
    $nodes[$nid]->cpd_attached_sets = $item;
  }

  return ;
}

/**
* Implementation of hook_node_view
*/

/*Hopefully all I need to do is create render arrays and let the theme functions do the
rest.*/

/*NOTE: There may be an issue with changes to the node object of this function not sticking.
If that is the case than maybe I need to use hook_node_view_alter or hook_view.*/

function cpdnode_node_view($node, $build_mode = 'full') {

  //Only display the cpd data if the build mode is full, otherwise don't display it.
  if($build_mode == 'full') {

    //Check to see if this node has cpd data attached to it.
    //If it hasn't continue. The cpd data would have been loaded during
    //the hook_node_load function.
    if(!isset($node->cpd_attached_sets)) {
       return;
    }

    //Get the cpd entity data and place it in more convenient variables.
    $e_date = $node->cpd_attached_sets->e_date;
    $value = $node->cpd_attached_sets->value;

    //Get the metric information Attach the metric suffix to the end.
    $metric_id = variable_get('cpd_core_global_metric', 1);
    $unit_name = cpd_core_get_metric_unit_name($metric_id);

    //Grab all of the cpds and elements attached to this item.
    //Copied from cpdnode.

    $all_cpds = unserialize($node->cpd_attached_sets->elements);

    //If there are no cpds attached to this item then set the has_cpds
    //flag to FALSE. This affects how the render arrays are built.
    $has_cpds = TRUE;
    if (empty($all_cpds)) {
      $has_cpds = FALSE;
    }

    //Create the render array we will attach to the node.
    $render = array();

    $render['item_metadata'] = array(
      '#theme' => 'cpdnode_nodemetadata_view',
      'e_date' => $e_date,
      'credit' => $value,
      'unit' => $unit_name,
      'has_cpds' => $has_cpds,
    );

    //Now add the elements.
    $render['elements'] = array();

    //Loop over all the cpds adding them onto the page.
    foreach ($all_cpds as $set_id => $cpd_items) {

      //Get the set name.
      $set_name = cpd_core_get_set_name($set_id);
      //Add the set name to the render array.
      $set_key = 'set_' . $set_id;
      $render[$set_key] = array(
        '#theme' => 'cpdnode_set_view',
        'set_id' => $set_id,
        'name' => $set_name,
        'cpds' => array(),
      );

      //Loop over all the cpd items adding them to the page.
      foreach ($cpd_items as $cpd_id => $elements) {
        //Given the cpd_id obtain the standard/cpd name.
        $cpd_name = cpd_core_get_cpd_name($cpd_id);

        //Add the cpd to the page.
        $cpd_key = 'cpd_' . $cpd_id;
        $render[$set_key]['cpds'][$cpd_key] = array(
          '#theme' => 'cpdnode_standard_view',
          'cpd_id' => $cpd_id,
          'name' => $cpd_name,
          'elements' => array(),
        );

        //Loop through each of the element ids adding them to the render
        //array.
        foreach ($elements as $key => $element_id) {
          $element_name = cpd_core_get_element_name($element_id);
          $element_key = 'element_' . $element_id;
          $render[$set_key]['cpds'][$cpd_key]['elements'][$element_key] = array(
            '#theme' => 'cpdnode_element_view',
            'e_id' => $element_id,
            'name' => $element_name,
          );
        }
      }
    }

    //Attach the render array to node->content.
    $node->content['cpd_attached_sets'] = $render;

  }

  return;

}
