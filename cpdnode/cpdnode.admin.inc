<?php

/**
 * @file
 * This file contains the CPD Node Administration functions.
 * These functions are an improvement on the Drupal 6 version which
 * did not have any administration and collection views for the CPD
 * items attached to nodes. A lot of code here is a duplicate of the
 * admin interface of CPD Custom.
 */

/*********************************************************/
/* Administration Pages. */
/*********************************************************/

/*Code to create the CPD custom item administration page.*/

/*This page is just one big table which lists all the custom CPD items
 along with options to edit and delete each item.*/

function cpdnode_admin_cpd_page() {

  drupal_set_title(t('CPD Node Items Administration'), PASS_THROUGH);

  //Create the render array for the output.
  $page_content = array();

  //Add the create new CPD set link.
  $page_content['intro'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('Manage the CPD node items for the whole site from this page.') . '</p><p>' . l(t('Add new CPD Node Item'), 'admin/cpdsuite/cpdnode_admin/cpd/add') . '</p><p>' . l(t('Set the allowed node types'), 'admin/cpdsuite/cpdnode_admin/settypes') . '</p><p>',
  );

  //Create the common table headers.
  $table_headers = array();
  $table_headers[] = t('Item Name');
  $table_headers[] = t('Node id');
  $table_headers[] = t('Node title');
  $table_headers[] = t('Date');
  $table_headers[] = t('Value');
  //Add the operation links.
  $table_headers[] = array('data' => t('Operations'), 'colspan' => '3');

  //Obtain all of the cpd items using a query which can be used
  //with a pager object.
  $query = db_select('cpdnode', 'cpdn')->extend('PagerDefault');
  $query->fields('cpdn', array('cne_id', 'nid', 'name', 'e_date', 'value'));
  $all_items = $query
    ->limit(50)         // this is where you change the number of rows
    ->orderBy('cpdn.cne_id')
    ->execute();

  //Test to see what I come back with.
  //print_r($all_items);
  //exit();
  //If there are no items display just the add item link and return.
  if (empty($all_items)) {
    return $page_content;
  }

  //Reached here then we have items to display. We will display the
  //items in a table.

  //Create the normal rows for the table.
  $rows = array();

  //Now using a foreach we build the rows. Note that the results array is no longer
  //indexed by primary key. Therefore we need to extract the primary key from the
  //item_data before creating the links.
  foreach ($all_items as $item_id => $item_data) {
    $cne_id = $item_data->cne_id;
    $view_item_link = 'admin/cpdsuite/cpdnode_admin/cpd/' . $cne_id . '/view';
    $edit_item_link = 'admin/cpdsuite/cpdnode_admin/cpd/' . $cne_id . '/edit';
    $delete_item_link = 'admin/cpdsuite/cpdnode_admin/cpd/' . $cne_id . '/delete';
    $item_name = check_plain($item_data->name);
    $nid = check_plain($item_data->nid);
    $node_title_raw = cpdnode_get_nodetitle($nid);
    $node_title = check_plain($node_title_raw);
    $event_date = date('d-m-Y', $item_data->e_date);
    $value = check_plain($item_data->value);

    //Create the row.
    $rows[] = array(
      $item_name,
      $nid,
      $node_title,
      $event_date,
      $value,
      l(t('View'), $view_item_link),  
      l(t('Edit'), $edit_item_link), 
      l(t('Delete'), $delete_item_link),
    );
  }

  //Create a render array which will be themed as a table with a pager.
  $page_content['pager_table'] = array(
    '#theme' => 'table', 
    '#header' => $table_headers, 
    '#rows' => $rows, 
    '#empty' => t('No users are tracking CPDs at this time.'),
  );

  //Attach the pager theme
  $page_content['pager_pager'] = array('#theme' => 'pager');

  return $page_content;
}

/**
 * Page to create the Set the Allowed Node Types page. At present this
 * page is on the CPD Node admin page, however I may move this to the
 * configuration section.
 * @return
 *   Returns a render array containing the form.
 */

function cpdnode_admin_cpd_settypes() {
  //Create the page title.
  drupal_set_title(t('Set the Allowed Node Types'), PASS_THROUGH);

  //Define the page content render array.
  $page_content = array();

  //Add the form to the page.
  $page_content['form'] = drupal_get_form('cpdnode_admin_cpd_settypes_form');

  return $page_content;
}

/**
 * Function to create the Set the Allowed Node Types form.
 * @param $form_state
 *   The current state of the form. Useful if we must return after an error.
 * @return
 *   Returns the form as a render array.
 */

function cpdnode_admin_cpd_settypes_form($form_state) {

    $nodetypes = node_type_get_names();
    $default_options = variable_get('cpdnode_allowed_types');
    if (empty($default_options)) {
       $default_options = array();
    }

    //Create the checkboxes for types.
    $form['node_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Allowed node types'),
      '#default_value' => $default_options,
      '#options' => $nodetypes,
      '#description' => t('Select the types of nodes you wish to allow CPDs to be attached to.'),
    );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/*The submit handler for the add cpd set form. Added as part of hook_form_alter
shown above.*/
function cpdnode_admin_cpd_settypes_form_submit($form, &$form_state) {
  //Grab the returned options. Checked options have the value
  //set to the name of the option and unchecked options have
  //the value zero.

  $checked_options = $form_state['values']['node_types'];

  //Strip out the keys with zero values. All we need is the selected type keys
  //for saving into our variable. The blasted zero is a string!
  $types = array();
  foreach ($checked_options as $key => $value) {
    if ($value != "0") {
      $types[] = $key;
    }
  }

  //Update the cpdnode_allowed_types variable.
  variable_set('cpdnode_allowed_types', $types);

  drupal_set_message(t('Allowed Node Types list updated.'), 'status');
  $form_state['redirect'] = 'admin/cpdsuite/cpdnode_admin';

  return ;

}

function cpdnode_admin_add_cpd_page() {
  //Create the page title.
  drupal_set_title(t('Add a new CPD Node Item'), PASS_THROUGH);

  //Define the page content render array.
  $page_content = array();

  //Add the form to the page.
  $page_content['form'] = drupal_get_form('cpdnode_admin_add_item_form');

  return $page_content;
}

/*Function to create the CPD Custom Item addition form for
administrators. This form differs from the user form in one component,
the user select field.*/

function cpdnode_admin_add_item_form($form_state) {

  //Go grab the names of all the available nodes in the ddatabase.
  $all_nodes = _cpdnode_get_nodes_without_cpds();
  //$all_nodes = _cpdnode_get_all_nodes();

  //The array which holds the form.
  $form['node_id'] = array(
      '#type' => 'select',
      '#title' => t('Node Title'),
      '#default_value' => array_map('check_plain', $all_nodes),
      '#options' => $all_nodes,
    '#description' => t('Select the node to apply this item to.'),
  );

  //Set the default value for the event date.
  //The default value is the first of January in the current year.
  $curr_year = date('Y');

  $form['e_date'] = array(
  '#type' => 'date',
  '#title' => t('Date of Event or Activity'),
  '#default_value' => array('day' => 1, 'month' => 1, 'year' => $curr_year),
  '#required' => TRUE,
  '#description' => "",
  );

  //Add the cpd form. Use blank fields for the default values (we're adding a
  //new cpd entity so no defaults).
  $form[] = cpdcore_create_cpdsets_form('', array());

  $form['add_button'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  //Manually add the default submit function.
  //$form['#submit'][] = 'cpd_direct_admin_add_cpd_item_form_submit';

  return $form;

}

/*The submission handler for the admin custom item addition form. Again
draw inspiration from the cpdnode module.*/

function cpdnode_admin_add_item_form_submit($form, &$form_state) {
  //If the Add button was clicked then we add this item.
  if ($form_state['clicked_button']['#value'] == t('Add')) {
    //Extract the selected elements from the form state.
    $elements_complete = cpdcore_form_state_to_sets_array($form_state);

    //Get the item value.
    $value = $form_state['values']['item_value'];

    //Get the date information. A feature of mktime is that its
    //arguments are in American date order. Has caused numerous
    //errors. We store dates as UNIX timestamps.
    $e_date_raw = $form_state['values']['e_date'];
    $e_date = mktime(0, 0, 0, $e_date_raw['month'], $e_date_raw['day'], $e_date_raw['year']);

    //Create the item structure. This is the array that will be passed to
    //entity_create in the values parameter. I have decided to store the
    //element data as a serialised array.
    $nid = $form_state['values']['node_id'];
    $item = array(
      'name' => $form['node_id']['#options'][$nid],
      'nid' => $nid,
      'e_date' => $e_date,
      'value' => $value,
      'elements' => $elements_complete,
    );

    //Create the custom cpd item entity and save it into the database.
    $custom_entity = entity_create('cpdnode', $item);

    //Save the new entity.
    $return_value = entity_save('cpdnode', $custom_entity);

    //$return_value = _cpdnode_save_item('add', $item);

    if ($return_value == FALSE) {
      $form_state['redirect'] = 'admin/cpdsuite/cpdnode_admin/cpd/add';
    }
    else {
      drupal_set_message(t('New Item added.'), 'status');
      $form_state['redirect'] = 'admin/cpdsuite/cpdnode_admin';
    }

  }

  return ;

}

/*TODO: Move most of this code into a theme function and then into a
template file.*/

function cpdnode_admin_view_cpd_page($item_id) {

  //Load the cpd custom item entity. We don't make use of the extra
  //parameters for entity_load as yet. The function entity_load returns
  //an array of entity objects. As we are only retrieving a single entity
  //we need to extract that object from the array.
  $item_arr = entity_load('cpdnode', array($item_id));
  
  //If item_arr is empty then this entity id is invalid. Set a message
  //and return the empty array.
  if (empty($item_arr)) {
    drupal_set_message(t('Sorry, this CPD item does not exist.'), 'warning');
    return array();
  }

  //Extract the lone entity from the array item_arr.
  $item = $item_arr[$item_id];

  //Get the information about the item with given item id.
  $name = $item->name;
  $nid = $item->nid;
  $nodetitle = cpdnode_get_nodetitle($item->nid);
  $e_date = $item->e_date;
  $value = $item->value;

  //Grab all of the cpds and elements attached to this item.
  //Copied from cpdnode.

  $all_cpds = $item->elements;

  //If there are no cpds attached to this item then set the has_cpds
  //flag to FALSE.
  $has_cpds = TRUE;
  if (empty($all_cpds)) {
    $has_cpds = FALSE;
  }

  //Add the title.
  $title_string = t('CPD Node Item - %name', array('%name' => $name));
  drupal_set_title($title_string, PASS_THROUGH);

  //Add the value for this item. Attach the metric suffix to the end.
  $metric_id = variable_get('cpdcore_global_metric', 1);
  $unit_name = cpdcore_get_metric_unit_name($metric_id);

  //Create the render array for the page content.
  $page_content = array();

  //Add the basic information.
  $page_content['item_metadata'] = array(
    '#theme' => 'cpdnode_admin_view',
    'name' => $name,
    'nid' => $nid,
    'nodetitle' => $nodetitle,
    'e_date' => $e_date,
    'credit' => $value,
    'unit' => $unit_name,
    'has_cpds' => $has_cpds,
  );

  //Create the render array for any attached cpd sets and add them
  //to the page content array.
  $page_content[] = cpdcore_create_cpdsets_render_array($all_cpds);

  return $page_content;
}

function cpdnode_admin_edit_cpd_page($item_id) {
  //Get the name of the CPD.
  $name = _cpdnode_get_item_name($item_id);
  if ($name == FALSE) {
    drupal_set_message(t('Item does not exist in the database'), 'warning');
    //Redirect to the admin page.
    //$form_state['#redirect'] = 'cpdcore_admin_cpd_page';
    return array();
  }

  //Create the page title.
  $titlestring = t('Edit CPD Custom Item') . ' ' . check_plain($name);
  drupal_set_title($titlestring, PASS_THROUGH);

  //Define the page content array.
  $page_content = array();

  //Add the form to the page.
  $page_content['form'] = drupal_get_form('cpdnode_admin_edit_item_form', $item_id);

  return $page_content;
}

/**
* Function to create the edit item form. This form is the same as the
* add form except that we add values already stored in the database
* as default values. The unused node argument is now mandatory otherwise
* the function signature is wrong and we cannot access the item_id arguemnt.
* @param $node
*    An unused argument.
* @param $form_state
*    The current state of the form. Useful when returning to the form after
*    an error.
* @param $item_id
*    The id of the entity we wish to look up.
*/

function cpdnode_admin_edit_item_form($node, $form_state, $item_id) {

  //Load the entity.
  $item_arr = entity_load('cpdnode', array($item_id));
  
  //If item_arr is empty then this entity id is invalid. Set a message
  //and return the empty array.
  if (empty($item_arr)) {
    drupal_set_message(t('Sorry, this CPD item does not exist.'), 'warning');
    return array();
  }

  //Extract the lone entity from the array item_arr.
  $item = $item_arr[$item_id];

  //Get the information about the item with given item id.
  $name = $item->name;
  $nid = $item->nid;
  $e_date = _cpdnode_timestamp2array($item->e_date, TRUE);
  $value = $item->value;

  //Grab all of the cpds and elements attached to this item.
  //Copied from cpdnode.

  $entity_cpds = $item->elements;

  //Go grab the names of all the nodes without CPDs
  //in the database.
  $all_nodes = _cpdnode_get_nodes_without_cpds();

  //Add the currently attached node as an option. We only do this
  //because we are on the edit form (we need to be able to select
  //the currently attached node from the dropdown menu).

  $all_nodes[$nid] = cpdnode_get_nodetitle($nid);

  //The array which holds the form.
  $form['item_name'] = array(
  '#type' => 'textfield',
  '#title' => t('Item Name'),
  '#default_value' => check_plain($name),
  '#required' => TRUE,
  '#description' => "Please enter the name of your item.",
  '#size' => 40,
  '#maxlength' => 255,
  );

  $form['node_id'] = array(
      '#type' => 'select',
      '#title' => t('Node Title'),
      '#default_value' => $nid,
      '#options' => $all_nodes,
    '#description' => t('Select the node to apply this item to.'),
  );

  $form['e_date'] = array(
  '#type' => 'date',
  '#title' => t('Date of Activity'),
  '#default_value' => $e_date,
  '#required' => TRUE,
  '#description' => "",
  );

  //Add the cpd form.
  $form[] = cpdcore_create_cpdsets_form($value, $entity_cpds);

  //Add the item_id as a value. Will be needed in the submit function.
  $form['item_id'] = array(
    '#type' => 'value',
    '#value' => $item_id,
  );

  $form['edit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;

}

/*The submission handler for the admin custom item edit form. This form
is the same as the add item form we just include the item id as part of
the item structure. Again draw inspiration from the cpdnode module.*/

function cpdnode_admin_edit_item_form_submit($form, &$form_state) {
  //If the Add button was clicked then we add this item.
  if ($form_state['clicked_button']['#value'] == t('Save')) {

    //Extract the selected elements from the form state.
    $elements_complete = cpdcore_form_state_to_sets_array($form_state);

      //Get the item value.
      $value = $form_state['values']['item_value'];

    //Get the date information. A feature of mktime is that its
    //arguments are in American date order. Has caused numerous
    //errors. We store dates as UNIX timestamps.
    $e_date_raw = $form_state['values']['e_date'];
    $e_date = mktime(0, 0, 0, $e_date_raw['month'], $e_date_raw['day'], $e_date_raw['year']);


    //Create the item structure
    $item = array(
      'cne_id' => $form_state['values']['item_id'],
      'name' => $form_state['values']['item_name'],
      'nid' => $form_state['values']['node_id'],
      'e_date' => $e_date,
      'value' => $value,
      'elements' => $elements_complete,
    );

    //Create the node cpd item entity and save it into the database.
    $node_entity = entity_create('cpdnode', $item);

    //To alert the save routine that we are updating an existing entity
    //unset the is_new variable.
    unset($node_entity->is_new);

    //Save the new entity.
    $return_value = entity_save('cpdnode', $node_entity);

    //$return_value = _cpd_direct_save_item('update', $item);

    if ($return_value == FALSE) {
      drupal_set_message(t('An error occured. Changes not saved'), 'error');
      $form_state['redirect'] = 'admin/cpdsuite/cpdnode_admin/cpd/' . check_plain($form_state['values']['item_id']) . '/edit';
    }
    else {
      drupal_set_message(t('Item updated.'), 'status');
      $form_state['redirect'] = 'admin/cpdsuite/cpdnode_admin/cpd/' . check_plain($form_state['values']['item_id']) . '/view';
    }

  }

  return ;

}

function cpdnode_admin_delete_cpd_confirm_page($item_id) {
  return drupal_get_form('cpdnode_admin_delete_item_confirm', $item_id);
}

/**
 * Function to create the delete confirmation form. Like the edit form
 * builder I need to assign the not-very-useful node parameter to get at
 * my item_id argument.
 * @param $node
 *   A non-useful first argument
 * @param $form_state
 *   The current state of the form. Useful when resetting after an error.
 * @param $item_id
 *   The id of the item we wish to delete.
 * @return
 *   Returns a form array containing the delete confirmation form.
 */

function cpdnode_admin_delete_item_confirm($node, $form_state, $item_id) {
  //Get the cpd name from the database, this will be used in the
  //confirmation message.

  $name = _cpdnode_get_item_name($item_id);
  if ($name == FALSE) {
    drupal_set_message(t('Item does not exist in the database'), 'warning');
    //Redirect to the admin page.
    //$form_state['#redirect'] = 'cpdcore_admin_cpd_page';
  }

  //Need to send the element_id along with the form.
  $form['item_id'] = array(
    '#type' => 'value',
    '#value' => $item_id,
  );

  //Return the confirmation form.
  return confirm_form($form,
    t('Are you sure you want to delete the Item %title?', array(
      '%title' => $name)),
    'admin/cpdsuite/cpdnode_admin',
    t('Once deleted this item cannot be retrieved.'),
    t('Delete'),
    t('Cancel')
  );
  return ;
}

/*The submit handler for the admin delete item form.*/

function cpdnode_admin_delete_item_confirm_submit($form, &$form_state) {
  //Create the item array. For deletes it only contains the one
  //item.
  $item = array('item_id' => $form_state['values']['item_id']);
  $item_id = $form_state['values']['item_id'];

  //Call the entity delete function.
  $return_value = entity_delete('cpdnode', $item_id);

  //Unfortunately entity_delete only returns FALSE in this case so I cannot
  //determine whether the delete succeeded just form the return value.
  //Assuming success and continuing anyway.
  drupal_set_message(t('Item deleted'), 'status');
  $form_state['redirect'] = 'admin/cpdsuite/cpdnode_admin';

  return ;
}
