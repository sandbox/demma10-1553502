<?php
/**
 * @file
 * The installation file of CPD Custom.
 */

/*Define the database schemas for the cpdcustom eentity.*/
function cpdcustom_schema() {

  $schema['cpdcustom'] = array(
  'description' => t('The base table for storing CPD custom entities.'),
    'fields' => array(
       'cce_id' => array(
            'description' => t('The primary identifier for this entity.'),
            'type' => 'serial',
            'unsigned' => TRUE,
            'not null' => TRUE,
       ),
       'uid' => array(
        'description' => t('The id of the user this entity belongs to.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => t('The name of the custom CPD item.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => t('An optional description for this item.'),
        'type' => 'text',
        'size' => 'medium',
        'not null' => FALSE,
      ),
      'e_date' => array(
        'description' => t('The date that this CPD was issued, in most cases this is the date that the event which provides the CPD occured. Stored as a UNIX timestamp.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
       ),
      'value' => array(
        'description' => t('The value or credit for this item or activity'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'elements' => array(
		    'type' => 'text',
		    'not null' => FALSE,
		    'size' => 'big',
			  'serialize' => TRUE,
			  'description' => 'A serialized array of elements of CPD standards attached to this entity.',
      ),
    ),
  'primary key' => array('cce_id'),
);

 return $schema;

}

/**
 * Implementation of hook_install.
 * Even though this function does nothing it is still needed to trigger
 * the call to drupal_install_schema.
 */

function cpdcustom_install() {
  // The drupal_(un)install_schema functions are called automatically in D7.
  return;
}

/**
 * Implementation of hook_uninstall.
 * Even though this is empty it is still needed to trigger the call to
 * drupal_uninstall_schema.
 */
function cpdcustom_uninstall() {
  // The drupal_(un)install_schema functions are called automatically in D7.
  return;
}

/**
* Add the 'description' column to the cpdcustom table.
*/
function cpdcustom_update_7100() {
  $spec = array(
    'description' => t('An optional description for this item.'),
    'type' => 'text',
    'size' => 'medium',
    'not null' => FALSE,
  );

  //Add the new field to the database table.
  db_add_field( 'cpdcustom', 'description', $spec);

}

