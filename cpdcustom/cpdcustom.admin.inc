<?php

/**
 * @file
 * This file contains the CPD Administration functions.
 */

/*********************************************************/
/* Administration Pages. */
/*********************************************************/

/*Code to create the CPD custom item administration page.*/

/*This page is just one big table which lists all the custom CPD items
 along with options to edit and delete each item.*/

function cpdcustom_admin_cpd_page() {

  drupal_set_title(t('CPD Custom Items Administration'), PASS_THROUGH);

  //Create the render array for the output.
  $page_content = array();

  //Add the create new CPD set link.
  $page_content['intro'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('Manage the custom CPD items for the whole site from this page.') . '</p><p>'. l('Add new CPD Custom Item', 'admin/cpdsuite/cpdcustom_admin/cpd/add') . '</p>',
  );

  //Create the common table headers.
  $table_headers = array();
  $table_headers[] = t('Item Name');
  $table_headers[] = t('User Name');
  $table_headers[] = t('Date');
  $table_headers[] = t('Value');
  //Add the operation links.
  $table_headers[] = array('data' => t('Operations'), 'colspan' => '3');

  //Obtain all of the cpd items both name user and id.
  //The query is dynamic to get the pager to work.
  $query = db_select('cpdcustom', 'cpdc')->extend('PagerDefault');
  $query->fields('cpdc', array('cce_id', 'uid', 'name', 'e_date', 'value'));
  $all_items = $query
    ->limit(50)         // this is where you change the number of rows
    ->orderBy('cpdc.cce_id')
    ->execute();

  //Test to see what I come back with.
  //print_r($all_items);
  //exit();
  //If there are no items display just the add item link and return.
  if (empty($all_items)) {
    return $page_content;
  }

  //Reached here then we have items to display. We will display the
  //items in a table.

  //Create the normal rows for the table.
  $rows = array();

  //Now using a foreach we build the rows. Note that the results array is no longer
  //indexed by primary key. Therefore we need to extract the primary key from the
  //item_data before creating the links.
  foreach ($all_items as $item_id => $item_data) {
    $cce_id = $item_data->cce_id;
    $view_item_link = 'admin/cpdsuite/cpdcustom_admin/cpd/' . $cce_id . '/view';
    $edit_item_link = 'admin/cpdsuite/cpdcustom_admin/cpd/' . $cce_id . '/edit';
    $delete_item_link = 'admin/cpdsuite/cpdcustom_admin/cpd/' . $cce_id . '/delete';
    $item_name = check_plain($item_data->name);
    $user_name_raw = cpdcore_get_username($item_data->uid);
    $user_name = check_plain($user_name_raw);
    $event_date = date('d-m-Y', $item_data->e_date);
    $value = check_plain($item_data->value);

    //Create the row.
    $rows[] = array(
      $item_name,
      $user_name,
      $event_date,
      $value,
      l('View', $view_item_link),  
      l('Edit', $edit_item_link), 
      l('Delete', $delete_item_link),
    );
  }

  //Create a render array which will be themed as a table with a pager.
  $page_content['pager_table'] = array(
    '#theme' => 'table', 
    '#header' => $table_headers, 
    '#rows' => $rows, 
    '#empty' => t('No users are tracking CPDs at this time.'),
  );

  //Attach the pager theme
  $page_content['pager_pager'] = array('#theme' => 'pager');

  return $page_content;
}

function cpdcustom_admin_add_cpd_page() {
  //Create the page title.
  drupal_set_title(t('Add a new CPD Custom Item'), PASS_THROUGH);

  //Define the page content render array.
  $page_content = array();

  //Add the form to the page.
  $page_content['form'] = drupal_get_form('cpdcustom_admin_add_item_form');

  return $page_content;
}

/*Function to create the CPD Custom Item addition form for
administrators. This form differs from the user form in one component,
the user select field.*/

function cpdcustom_admin_add_item_form($form_state) {
  //Go grab the names of all the metrics in the
  //database.
  $all_users = _cpdcustom_get_all_users();

  //The array which holds the form.
  $form['item_name'] = array(
  '#type' => 'textfield',
  '#title' => t('Item Name'),
  '#default_value' => "",
  '#required' => TRUE,
  '#description' => "Please enter the name of your item.",
  '#size' => 40,
  '#maxlength' => 255,
  );

  $form['user_id'] = array(
      '#type' => 'select',
      '#title' => t('User Name'),
      '#default_value' => array_map('check_plain', $all_users),
      '#options' => $all_users,
    '#description' => t('Select the user to apply this item to.'),
  );

  //Set the default value for the event date.
  //The default value is the first of January in the current year.
  $curr_year = date('Y');

  $form['e_date'] = array(
  '#type' => 'date',
  '#title' => t('Date of Event or Activity'),
  '#default_value' => array('day' => 1, 'month' => 1, 'year' => $curr_year),
  '#required' => TRUE,
  '#description' => "",
  );

  $form['item_description'] = array(
  '#type' => 'textarea',
  '#title' => t('Description'),
  '#required' => FALSE,
  '#description' => "Provide details about the actitiy you are entering. This can include a summary of the event or an outline of the program of the activity. If the activity involved professional reading this field can be used to list the articles and/or journals read.",
  );

  //Add the cpd form.
  $form[] = cpdcore_create_cpdsets_form('', array());

  $form['add_button'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;

}

/*The submission handler for the admin custom item addition form. Again
draw inspiration from the cpd_node module.*/

function cpdcustom_admin_add_item_form_submit($form, &$form_state) {
  //If the Add button was clicked then we add this item.
  if ($form_state['clicked_button']['#value'] == t('Add')) {

    //Extract the selected elements from the form state.
    $elements_complete = cpdcore_form_state_to_sets_array($form_state);

    //Get the item value.
    $value = $form_state['values']['cpd_item_value'];

    //Get the date information. A feature of mktime is that its
    //arguments are in American date order. Has caused numerous
    //errors. We store dates as UNIX timestamps.
    $e_date_raw = $form_state['values']['e_date'];
    $e_date = mktime(0, 0, 0, $e_date_raw['month'], $e_date_raw['day'], $e_date_raw['year']);

    //Create the item structure. This is the array that will be passed to
    //entity_create in the values parameter. I have decided to store the
    //element data as a serialised array.
    $item = array(
      'name' => $form_state['values']['item_name'],
      'uid' => $form_state['values']['user_id'],
      'e_date' => $e_date,
      'description' => $form_state['values']['item_description'],
      'value' => $value,
      'elements' => $elements_complete,
    );

    //Create the custom cpd item entity and save it into the database.
    $custom_entity = entity_create('cpdcustom', $item);

    //Save the new entity.
    $return_value = entity_save('cpdcustom', $custom_entity);

    //If CPD Tracker is enabled automatically add this item to the
    //user's tracker report. Also only execute this if entity_save
    //succeeded.
    if ($return_value != FALSE && module_exists('cpdtracker')) {
      $id = $custom_entity->cce_id;
      $uid = $custom_entity->uid;
      cpdtracker_add_tracking_item($uid, $id, 'custom');
    }

    //$return_value = _cpdcustom_save_item('add', $item);

    if ($return_value == FALSE) {
      $form_state['redirect'] = 'admin/cpdsuite/cpdcustom_admin/cpd/add';
    }
    else {
      drupal_set_message(t('New Item added.'), 'status');
      $form_state['redirect'] = 'admin/cpdsuite/cpdcustom_admin';
    }

  }

  return ;

}

/*TODO: Move most of this code into a theme function and then into a
template file.*/

function cpdcustom_admin_view_cpd_page($item_id) {

  //Load the cpd custom item entity. We don't make use of the extra
  //parameters for entity_load as yet. The function entity_load returns
  //an array of entity objects. As we are only retrieving a single entity
  //we need to extract that object from the array.
  $item_arr = entity_load('cpdcustom', array($item_id));
  
  //If item_arr is empty then this entity id is invalid. Set a message
  //and return the empty array.
  if(empty($item_arr)) {
    drupal_set_message(t('Sorry, this CPD item does not exist.'), 'warning');
    return array();
  }

  //Extract the lone entity from the array item_arr.
  $item = $item_arr[$item_id];

  //Get the information about the item with given item id.
  $name = $item->name;
  $username = cpdcore_get_username($item->uid);
  $description = $item->description;
  $e_date = $item->e_date;
  $value = $item->value;

  //Grab all of the cpds and elements attached to this item.
  //Copied from cpd_node.
  //See cpdcustom_user_view_cpd_page() for an explanation of
  //why there is a check for if the data is serialised.

  $all_cpds = $item->elements;
  if(_cpdcustom_is_serial($all_cpds) == TRUE) {
    $all_cpds = unserialize($all_cpds);
  }

  //If there are no cpds attached to this item then set the has_cpds
  //flag to FALSE.
  $has_cpds = TRUE;
  if(empty($all_cpds)) {
    $has_cpds = FALSE;
  }

  //Add the title.
  $title_string = t('CPD Custom Item - %name', array('%name' => $name));
  drupal_set_title($title_string, PASS_THROUGH);

  //Add the value for this item. Attach the metric suffix to the end.
  $metric_id = variable_get('cpdcore_global_metric', 1);
  $unit_name = cpdcore_get_metric_unit_name($metric_id);

  //Create the render array for the page content.
  $page_content = array();

  //Add the basic information.
  $page_content['item_metadata'] = array(
    '#theme' => 'cpdcustom_admin_view',
    'name' => $name,
    'username' => $username,
    'e_date' => $e_date,
    'description' => $description,
    'credit' => $value,
    'unit' => $unit_name,
    'has_cpds' => $has_cpds,
  );

  //Create the render array for any attached cpd sets and add them
  //to the page content array.
  $page_content[] = cpdcore_create_cpdsets_render_array($all_cpds);

  return $page_content;
}

function cpdcustom_admin_edit_cpd_page($item_id) {
  //Get the name of the CPD.
  $name = _cpdcustom_get_item_name($item_id);
  if ($name == FALSE) {
    drupal_set_message(t('Item does not exist in the database'), 'warning');
    //Redirect to the admin page.
    //$form_state['#redirect'] = 'cpdcore_admin_cpd_page';
    return array();
  }

  //Create the page title.
  $titlestring = t('Edit CPD Custom Item') . ' ' . check_plain($name);
  drupal_set_title($titlestring, PASS_THROUGH);

  //Define the page content array.
  $page_content = array();

  //Add the form to the page.
  $page_content['form'] = drupal_get_form('cpdcustom_admin_edit_item_form', $item_id);

  return $page_content;
}

/*Function to create the edit item form. This form is the same as the
add form except that we add values already stored in the database
as default values. The unused node argument is now mandatory otherwise
the function signature is wrong and we cannot access the item_id arguemnt.*/

function cpdcustom_admin_edit_item_form($node, $form_state, $item_id) {

  //Load the entity.
  $item_arr = entity_load('cpdcustom', array($item_id));
  
  //If item_arr is empty then this entity id is invalid. Set a message
  //and return the empty array.
  if(empty($item_arr)) {
    drupal_set_message(t('Sorry, this CPD item does not exist.'), 'warning');
    return array();
  }

  //Extract the lone entity from the array item_arr.
  $item = $item_arr[$item_id];

  //Get the information about the item with given item id.
  $name = $item->name;
  $uid = $item->uid;
  $description = $item->description;
  $e_date = cpdcore_timestamp2array($item->e_date);
  $value = $item->value;

  //Grab all of the cpds and elements attached to this item.
  //Copied from cpd_node.
  //See cpdcustom_user_view_cpd_page for an explanation of
  //why there is a check for serialised data here.

  $entity_cpds = $item->elements;
  if(_cpdcustom_is_serial($entity_cpds) == TRUE) {
    $entity_cpds = unserialize($entity_cpds);
  }

  //Go grab the names of all the metrics in the
  //database.
  $all_users = _cpdcustom_get_all_users();

  //The array which holds the form.
  $form['item_name'] = array(
  '#type' => 'textfield',
  '#title' => t('Item Name'),
  '#default_value' => check_plain($name),
  '#required' => TRUE,
  '#description' => "Please enter the name of your item.",
  '#size' => 40,
  '#maxlength' => 255,
  );

  $form['user_id'] = array(
      '#type' => 'select',
      '#title' => t('User Name'),
      '#default_value' => $uid,
      '#options' => $all_users,
    '#description' => t('Select the user to apply this item to.'),
  );

  $form['e_date'] = array(
  '#type' => 'date',
  '#title' => t('Date of Activity'),
  '#default_value' => $e_date,
  '#required' => TRUE,
  '#description' => "",
  );

  $form['item_description'] = array(
  '#type' => 'textarea',
  '#title' => t('Description'),
  '#default_value' => check_plain($description),
  '#required' => FALSE,
  '#description' => "Provide details about the actitiy you are entering. This can include a summary of the event or an outline of the program of the activity. If the activity involved professional reading this field can be used to list the articles and/or journals read.",
  );

  //Add the cpd form.
  $form[] = cpdcore_create_cpdsets_form($value, $entity_cpds);

  //Add the item_id as a value. Will be needed in the submit function.
  $form['item_id'] = array(
    '#type' => 'value',
    '#value' => $item_id,
  );

  $form['edit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;

}

/*The submission handler for the admin custom item edit form. This form
is the same as the add item form we just include the item id as part of
the item structure. Again draw inspiration from the cpd_node module.*/

function cpdcustom_admin_edit_item_form_submit($form, &$form_state) {
  //If the Add button was clicked then we add this item.
  if ($form_state['clicked_button']['#value'] == t('Save')) {
    //Create empty arrays for the cpd and elements data.
    $cpd_array = array();
    $elements = array();

    //Extract the selected elements from the form state.
    $elements_complete = cpdcore_form_state_to_sets_array($form_state);

      //Get the item value.
      $value = $form_state['values']['cpd_item_value'];

    //Get the date information. A feature of mktime is that its
    //arguments are in American date order. Has caused numerous
    //errors. We store dates as UNIX timestamps.
    $e_date_raw = $form_state['values']['e_date'];
    $e_date = mktime(0, 0, 0, $e_date_raw['month'], $e_date_raw['day'], $e_date_raw['year']);


    //Create the item structure
    $item = array(
      'cce_id' => $form_state['values']['item_id'],
      'name' => $form_state['values']['item_name'],
      'uid' => $form_state['values']['user_id'],
      'description' => $form_state['values']['item_description'],
      'e_date' => $e_date,
      'value' => $value,
      'elements' => $elements_complete,
    );

    //Create the custom cpd item entity and save it into the database.
    $custom_entity = entity_create('cpdcustom', $item);

    //To alert the save routine that we are updating an existing entity
    //unset the is_new variable.
    unset($custom_entity->is_new);

    //Save the new entity.
    $return_value = entity_save('cpdcustom', $custom_entity);

    //$return_value = _cpd_direct_save_item('update', $item);

    if ($return_value == FALSE) {
      drupal_set_message(t('An error occured. Changes not saved'), 'error');
      $form_state['redirect'] = 'admin/cpdsuite/cpdcustom_admin/cpd/' . check_plain($form_state['values']['item_id']) . '/edit';
    }
    else {
      drupal_set_message(t('Item updated.'), 'status');
      $form_state['redirect'] = 'admin/cpdsuite/cpdcustom_admin/cpd/' . check_plain($form_state['values']['item_id']) . '/view';
    }

  }

  return ;

}

function cpdcustom_admin_delete_cpd_confirm_page($item_id) {
  return drupal_get_form('cpdcustom_admin_delete_item_confirm', $item_id);
}

/**
 * Function to create the delete confirmation form. Like the edit form
 * builder I need to assign the not-very-useful node parameter to get at
 * my item_id argument.
 * @param $node
 *   A non-useful first argument
 * @param $form_state
 *   The current state of the form. Useful when resetting after an error.
 * @param $item_id
 *   The id of the item we wish to delete.
 * @return
 *   Returns a form array containing the delete confirmation form.
 */

function cpdcustom_admin_delete_item_confirm($node, $form_state, $item_id) {
  //Get the cpd name from the database, this will be used in the
  //confirmation message.

  $name = _cpdcustom_get_item_name($item_id);
  if ($name == FALSE) {
    drupal_set_message(t('Item does not exist in the database'), 'warning');
    //Redirect to the admin page.
    //$form_state['#redirect'] = 'cpdcore_admin_cpd_page';
  }

  //Need to send the element_id along with the form.
  $form['item_id'] = array(
    '#type' => 'value',
    '#value' => $item_id,
  );

  //Return the confirmation form.
  return confirm_form($form,
    t('Are you sure you want to delete the Item %title?', array(
      '%title' => $name)),
    'admin/content/cpdcustom_admin',
    t('Once deleted this item cannot be retrieved.'),
    t('Delete'),
    t('Cancel')
  );
  return ;
}

/*The submit handler for the admin delete item form.*/

function cpdcustom_admin_delete_item_confirm_submit($form, &$form_state) {
  //Create the item array. For deletes it only contains the one
  //item.
  $item = array('item_id' => $form_state['values']['item_id']);
  $item_id = $form_state['values']['item_id'];

  //Call the entity delete function.
  $return_value = entity_delete('cpdcustom', $item_id);

  //Unfortunately entity_delete only returns FALSE in this case so I cannot
  //determine whether the delete succeeded just form the return value.
  //Assuming success and continuing anyway.
  drupal_set_message(t('Item deleted'), 'status');
  $form_state['redirect'] = 'admin/cpdsuite/cpdcustom_admin';

  //Again assuming success delete any and all cpd tracker items linked to
  //this custom CPD item.
  cpdtracker_object_item_delete($item_id, 'custom');

  return ;
}
