<?php

/**
 * @file
 * The module file for the CPD Custom module. This module is the direct
 * successor to the CPD Direct module for Drupal 6. This module has been
 * reworked to take advantage of the new Entity API.
 */

/**
 * Implementation of hook_help
 */
function cpdcustom_help($path, $arg) {
  $output = '';
   switch ($path) {
    case "admin/help#cpdcustom":
      $output = '<p>' . t("The CPD Custom module allows users with appropriate permissions to enter in their own CPD items and attach CPD credit to them.") . '</p>';
      break;
  }
  return $output;
}

/**
 * Implementation of hook_permissions
 */
function cpdcustom_permission() {
  return array(
    'administer CPD custom items' => array(
      'title' => t('Administer CPD custom items'),
      'description' => t('Allow administration of all CPD custom items.'),
    ),
    'create own CPD custom items' => array(
      'title' => t('Create own CPD custom items'),
      'description' => t('Allow users to create and manage their own custom CPD items.'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 * @todo Extend the Entity class to handle the aggregation of element
 * information.
 */
function cpdcustom_entity_info() {
  $return = array(
    'cpdcustom' => array(
      'label' => t('CPD Custom Item Entity'),
      'entity class' => 'Entity',
      'controller class' => 'EntityAPIController',
      'base table' => 'cpdcustom',
      'fieldable' => FALSE,
      'view modes' => array(
        'cpdcustom_admin' => array(
          'label' => t('CPD Custom Admin'),
          'custom settings' => FALSE,
        ),
      ),
      'entity keys' => array(
        'id' => 'cce_id',
        'label' => 'name',
      ),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'cpdcustom_admin_access',
      'module' => 'cpdcustom',
      //'metadata controller class' => 'EntityMetadataController'
    ),
  );

  return $return;
}

/**
 * Implements hook_theme().
 */
function cpdcustom_theme() {
  return array(
    'cpdcustom_admin_view' => array(
      'render element' => 'elements',
      'template' => 'cpdcustom_metadata',
    ),
    'cpdcustom_user_view' => array(
      'render element' => 'elements',
      'template' => 'cpdcustom_metadata',
    ),
  );
}

/**
 * Administration interface access callback. Returns TRUE if the user has
 * permission to access the CPD Custom administration interface.
 */
function cpdcustom_admin_access($op, $type = NULL, $account = NULL) {
  return user_access('administer CPD custom items', $account);
}

/**
 * Implementation of hook_menu.
 * Add all of the custom CPD pages to the Drupal menu system.
 */

function cpdcustom_menu() {
  $items = array();

  //Set the url for the cpdcore_cpd_admin_page.
  //User must have the administer CPDs permission to
  //be able to use this setting.
  $items['admin/cpdsuite/cpdcustom_admin'] = array(
    'title' => 'Administer Custom CPD Items',
    'description' => 'Administer the CPD custom items that users have created.',
    'file' => 'cpdcustom.admin.inc',
    'page callback' => 'cpdcustom_admin_cpd_page',
    'access arguments' => array('administer CPD custom items'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/cpdsuite/cpdcustom_admin/cpd/add'] = array(
    'title' => 'Add new CPD custom item',
    'description' => 'Add a new CPD custom item for a user.',
    'file' => 'cpdcustom.admin.inc',
    'page callback' => 'cpdcustom_admin_add_cpd_page',
    'access arguments' => array('administer CPD custom items'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/cpdsuite/cpdcustom_admin/cpd/%'] = array(
    'title' => 'View',
    'description' => 'View the selected CPD custom item.',
    'file' => 'cpdcustom.admin.inc',
    'page callback' => 'cpdcustom_admin_view_cpd_page',
    'page arguments' => array(4),
    'access arguments' => array('administer CPD custom items'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/cpdsuite/cpdcustom_admin/cpd/%/view'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'View',
    'page arguments' => array(4),
    'weight' => 0,
  );
  $items['admin/cpdsuite/cpdcustom_admin/cpd/%/edit'] = array(
    'title' => 'Edit',
    'description' => 'Edit the selected CPD custom item.',
    'file' => 'cpdcustom.admin.inc',
    'page callback' => 'cpdcustom_admin_edit_cpd_page',
    'page arguments' => array(4),
    'access arguments' => array('administer CPD custom items'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/cpdsuite/cpdcustom_admin/cpd/%/delete'] = array(
    'title' => 'Delete',
    'description' => 'Delete the selected CPD custom item.',
    'file' => 'cpdcustom.admin.inc',
    'page callback' => 'cpdcustom_admin_delete_cpd_confirm_page',
    'page arguments' => array(4),
    'access arguments' => array('administer CPD custom items'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );

  //Links for the user with appropriate permissions to add their CPD. Be
  //aware that we do have code duplication here because these pages are
  //very similar to the admin pages (there may be a way to merge both
  //sets of pages).

  $items['cpdcustom_user'] = array(
    'title' => 'My custom CPD',
    'description' => 'Page for the user to administer their custom CPD custom items.',
    'file' => 'cpdcustom.user.inc',
    'page callback' => 'cpdcustom_user_cpd_page',
    'access arguments' => array('create own CPD custom items'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['cpdcustom_user/cpd/add'] = array(
    'title' => 'Add new CPD custom item',
    'description' => 'Allow user to add their CPD custom items.',
    'file' => 'cpdcustom.user.inc',
    'page callback' => 'cpdcustom_user_add_cpd_page',
    'access arguments' => array('create own CPD custom items'),
    'type' => MENU_CALLBACK,
  );
  $items['cpdcustom_user/cpd/%'] = array(
    'title' => 'View',
    'description' => 'Allow user to view their CPD custom items.',
    'file' => 'cpdcustom.user.inc',
    'page callback' => 'cpdcustom_user_view_cpd_page',
    'page arguments' => array(2),
    'access arguments' => array('create own CPD custom items'),
    'type' => MENU_CALLBACK,
  );
  $items['cpdcustom_user/cpd/%/view'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'View',
    'page arguments' => array(2),
    'weight' => 0,
  );
  $items['cpdcustom_user/cpd/%/edit'] = array(
    'title' => 'Edit',
    'description' => 'Edit the selected CPD custom item.',
    'file' => 'cpdcustom.user.inc',
    'page callback' => 'cpdcustom_user_edit_cpd_page',
    'page arguments' => array(2),
    'access arguments' => array('create own CPD custom items'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['cpdcustom_user/cpd/%/delete'] = array(
    'title' => 'Delete',
    'description' => 'Delete the selected CPD custom item.',
    'page callback' => 'cpdcustom_user_delete_cpd_confirm_page',
    'file' => 'cpdcustom.user.inc',
    'page arguments' => array(2),
    'access arguments' => array('create own CPD custom items'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );

  return $items;
}

/**
 * Function to load all of the CPD custom entities without
 * element information. This
 * function uses a custom database query instead of entity_load
 * because we do not need all of the available fields.
 * @return
 *  Returns an array of objects containing all of the CPD item metadata.
 */
function cpdcustom_get_all_items_metadata() {
  //Set the return array.
  $ret_val = array();

  //Set up the query and placeholder tokens array.
  $query = 'SELECT cce_id, uid, name, e_date, value FROM {cpdcustom}';
  $result = db_query($query);

  //Believe it or not, fetchAllAssoc returns an array of OBJECTS keyed
  //by the field provided in it's argument. NOT an array of arrays keyed
  //by the field provided in it's argument.
  $ret_val = $result->fetchAllAssoc('cce_id');

  //If there is an error or no records than fetchAll will return FALSE.
  //Send an empty array back to the caller in this case.
  if($ret_val == FALSE) {
    return array();
  }
  //Use the entity_load function to load all entities of our profile type.
  //$cce_ids = array();
  //$conditions = array();
  //$reset = FALSE;

  //$ret_val = entity_load('cpdcustom', FALSE, $conditions, $reset);

  return $ret_val;
}

/**
 * Function to load all of the CPD custom entities. This function
 * includes the loading of the element information.
 * @return
 *  Returns an array of CPD custom item entities.
 */
function cpdcustom_get_all_items() {
  //Use the entity_load function to load all entities of our profile type.
  $cce_ids = array();
  $conditions = array();
  $reset = FALSE;

  $ret_val = entity_load('cpdcustom', FALSE, $conditions, $reset);

  return $ret_val;
}

/**
 * Function to load all of the CPD custom entities belonging to a user without
 * element information. This
 * function uses a custom database query instead of entity_load
 * because we do not need all of the available fields.
 * @param $uid
 *   The uid of the user we wish to load entities for.
 * @return
 *  Returns an array of objects containing all of the CPD item metadata.
 */
function _cpdcustom_get_items_metadata_user($uid) {
  //Set the return array.
  $ret_val = array();

  //Set up the query and placeholder tokens array.
  $query = 'SELECT cce_id, uid, description, name, e_date, value FROM {cpdcustom} WHERE uid = :uid';
  $result = db_query($query, array(':uid' => $uid));

  //Believe it or not, fetchAllAssoc returns an array of OBJECTS keyed
  //by the field provided in it's argument. NOT an array of arrays keyed
  //by the field provided in it's argument.
  $ret_val = $result->fetchAllAssoc('cce_id');

  //If there is an error or no records than fetchAll will return FALSE.
  //Send an empty array back to the caller in this case.
  if($ret_val == FALSE) {
    return array();
  }

  return $ret_val;
}

/**
 * Function to create a list of all users (active and inactive) on the site. I find it hard
 * to believe that there is no API function to get a list of all users
 * on the site.
 * @return
 *   Returns an array of the form uid => user_name. Returns NULL on error.
 */

function _cpdcustom_get_all_users() {
  //Get all active users. As user is a core module we don't need it
  //as a dependency.
  $query = 'SELECT uid, name FROM {users}';
  $result = db_query($query);

  //If there are no results for the query then $result will be
  //empty. Return the empty array.
  if (count($result) <= 0) {
    return array();
  }
  //If something went wrong then $result will equal FALSE.
  //This is an error so we return NULL
  if ($result == FALSE) {
    return NULL;
  }

  //Fetch the results using fetchAllKeyed. The first column is uid and the
  //second column is username.
  $all_users = $result->fetchAllKeyed(0, 1);

  return $all_users;
}

/**
 * Function to create a list of all active users on the site. I find it hard
 * to believe that there is no API function to get a list of all users
 * on the site.
 * @return
 *   Returns an array of the form uid => user_name. Returns NULL on error.
 */

function _cpdcustom_get_all_active_users() {
  //Get all active users. As user is a core module we don't need it
  //as a dependency.
  $query = 'SELECT uid, name FROM {users} WHERE status = 1';
  $result = db_query($query);

  //If there are no results for the query then $result will be
  //empty. Return the empty array.
  if (count($result) <= 0) {
    return array();
  }
  //If something went wrong then $result will equal FALSE.
  //This is an error so we return NULL
  if ($result == FALSE) {
    return NULL;
  }

  //Fetch the results using fetchAllKeyed. The first column is uid and the
  //second column is username.
  $all_users = $result->fetchAllKeyed(0, 1);

  return $all_users;
}

/**
* Function which given an item_id returns the name of that item. For such a
* simple request we don't bother loading the entity, just do a static query.
* @param $item_id
*   The id of the entity we wish to look up. 
* @return
*   Returns the name if the element exists or FALSE otherwise.
*/

function _cpdcustom_get_item_name($item_id) {
  $query = 'SELECT name FROM {cpdcustom} WHERE cce_id = :id';

  $result = db_query_range($query, 0, 1, array(':id' => $item_id));

  //If result returns FALSE or the name is empty then we return
  //FALSE.
  if ($result == FALSE || count($result) <= 0) {
    return FALSE;
  }

  //Fetch the name.
  $res_obj = $result->fetchObject();

  return $res_obj->name;
}

/*Given an item_id return the uid of the owner. Returns the uid if the
 item exists or FALSE otherwise. For such a simple requrest don't bother
loading the entity, just do a static query.*/

function _cpdcustom_get_owner_id($item_id) {
  $query = 'SELECT uid FROM {cpdcustom} WHERE cce_id = :id';

  $result = db_query_range($query, 0, 1, array(':id' => $item_id));

  //If result returns FALSE or the name is empty then we return
  //FALSE.
  if ($result == FALSE || count($result) <= 0) {
    return FALSE;
  }

  //Fetch the name.
  $res_obj = $result->fetchObject();

  return $res_obj->uid;
}

/***********************************************/
/* CPD Tracker hook functions. */
/***********************************************/

/**
* Implementation of hook_cpd_grab_tracking_data_all.
* Returns an array of objects with the items with cpds attached to them that
* belong to this user. This is important as this module only makes data to
* the user. Remember that this hook is used in the Add CPD screen (another
* hook is used for the admin interface),

* The objects contain the following data members:
*  - item_id - the item id (used in tracker as obj_id)
*  - obj_type - the type this item is, in this case custom
*  - title - the name of the item, the custom item name.
*  - e_date - the date that this item's activity occured,
*  - value - the credit value for this item,
*
* @param $uid
*  A user id. The caller can specify which user to return CPD items
*  for. If this argument defaults to NULL than data for the currently
*  logged-in user is returned.
*/
function cpdcustom_cpd_grab_tracking_data_all($uid = NULL) {
  //The output array.
  $output = array();

  //Check to see if a user id is supplied. If it isn't then
  //we get the user id for the current user.
  if($uid == NULL) {
    //Get the current user.
    global $user;
    $uid = $user->uid;
  }

  //This time we're lucky that the metadata retrieval function already
  //exists. All I need to do is rename some of the object memebers.
  $objects_arr = _cpdcustom_get_items_metadata_user($uid);

  //If result is empty then there are no items for this user.
  if (empty($objects_arr)) {
    return $output;
  }

  //Loop through and create all of the output objects.

  foreach ($objects_arr as $cce_id => $obj) {
    //We're going to create an associative array with all the keys
    //that cpd_tracker is expecting. Then we will cast that array into
    //an object (forcing PHP to produce a standard class object). I've
    //found this to be the easiest way so far of creating generic objects,
    //however there may be a more optimal method out there.

    //Create our empty associative array.
    $res_array = array();

    //Set all of the object values with the appropriate keys.
    $res_array['obj_id'] = $cce_id;
    $res_array['title'] = $obj->name;
    $res_array['type'] = 'custom';
    $res_array['e_date'] = cpdcore_timestamp2array($obj->e_date);
    $res_array['value'] = $obj->value;

    //Turn the res_array into an object and append it to the output array.
    $output[] = (object) $res_array;
  }

  return $output;
}

/**
* Implementation of hook_cpd_grab_name_by_id.
* Only returns a value if the type is custom.
*/

function cpdcustom_cpd_grab_name_by_id($id, $type) {
  $title = '';
  //Only fire if type equals node.
  if ($type == 'custom') {
    //Get the custom item title.
    $title = _cpdcustom_get_item_name($id);
  }

  return $title;
}

/**
 * Implementation of hook_cpd_grab_tracking_data_item()
 */
function cpdcustom_cpd_grab_tracking_data_item($item_id, $type) {
  $res_array = array();

  //Only retrieve this item if it is of type 'custom'.
  if($type == 'custom') {
    //Load the entity.
    $item_arr = entity_load('cpdcustom', array($item_id));

    //If the entity does not exist in the database return null.
    if(empty($item_arr)) {
        return null;
    }

    //Entity_load returns an array of entities. Extract the sole entity.
    $item = $item_arr[$item_id];

    //Grab all the item data and transfer it to the result array.
    $res_array['name'] = $item->name;
    $res_array['e_date'] = cpdcore_timestamp2array($item->e_date);
    $res_array['e_timestamp'] = $item->e_date;
    $res_array['value'] = $item->value;
    $res_array['elements'] = $item->elements;

  }

  //Convert the result array to an object and return it. If it is empty return null.
  if(empty($res_array)) {
    return null;
  }

  return (object) $res_array;

}

/**
* Implementation of hook_cpd_grab_tracking_data_user.
* Get all the data from all the custom item objects in this array and
* create the output array in the form cpdtracker expects it to be.
* This form is array[set_id][item_id][field_key] = field_value where field_key is
* the name of one of the fields for this entity and field_value is the value
* of that field.*/
function cpdcustom_cpd_grab_tracking_data_user($data_objects) {

  //The output array.
  $output = array();

  //Loop through the data objects. We only act on objects of type node.
  foreach ($data_objects as $object) {
    if ($object->obj_type == 'custom') {
      //Taking the obj_id as the item id get the full item record.
      $item_id = $object->obj_id;

      //Load the CPD custom item entity.
      $item_arr = entity_load('cpdcustom', array($item_id));

      //If the entity does not exist in the database just continue.
      if(empty($item_arr)) {
        continue;
      }

      //Extract the lone entity from the array item_arr.
      $item = $item_arr[$item_id];

      //Extract all of the needed variables from the entity.
      $title = $item->name;
      $e_date = cpdcore_timestamp2array($item->e_date);

      //If the tracker object has overrides for the value
      //set use that. Otherwise use the value from the CPD entity.
      $value = $item->value;
      if(!empty($object->value_override)) {
        $value = $object->value_override;
      }

      //Grab all of the cpds and elements attached to this item.
      //Again if there is an override set for the elements in the
      //tracker entity then use the override. Otherwise use the one
      //from the CPD entity.
      $all_cpds_wsets = $item->elements;
      if(!empty($object->elements_override)) {
        $all_cpds_wsets = $object->elements_override;
      }

      //If the elements array is still serialised then unserialise it.
      if(_cpdcustom_is_serial($all_cpds_wsets)) {
        $all_cpds_wsets = unserialize($item->elements);
      }

      //Oh great a double foreach loop.
      foreach ($all_cpds_wsets as $set_id => $all_cpds) {
        //Add the special name value to the array.
        $output[$set_id][$item_id]['name'] = $title;
        $output[$set_id][$item_id]['e_date'] = $e_date;
        $output[$set_id][$item_id]['value'] = $value;

        //Add another special value to the array. This is because
        //module_invoke_all is rearranging my numerical indexes
        //and I am losing the set id.
        $output[$set_id]['set_id'] = $set_id;

        //Now add the new record to the output array.
        //The way we append the item makes
        //sure that the output array is in the correct form.

        $output[$set_id][$item_id]['elements'][$set_id] = $all_cpds;
        
      }
    }
  }

  return $output;
}

/**
 * Utility function to check whether or not a given string is a serialised
 * array. During normal module operation we should never need to store
 * boolean values in the serialised array, therefore we don't check for
 * the case where the data being returned is a boolean (every time FALSE
 * is returned we assume it's an error).
 * @param $data
 *   The input data we wish to test is serialised.
 * @return
 *   Returns FALSE if the input data is not serialised and TRUE if it is
 *   serialised.
 */
function _cpdcustom_is_serial( $data ) {
    $data = @unserialize($data);
    if( $data === false ) {
        return false;
    } else {
        return true;
    }
}  

/***********************************************/
/* CPD Archive Hook Functions. */
/***********************************************/

/**
 * Function to obtain a list of all CPD items which can be archived.
 * In this module all items in the cpdcustom table are candidates
 * for archiving.
 * @return
 *  Returns an associative array keyed by both the item type and the
 *  item id.
 */

function cpdcustom_cpdarchive_get_all_arc_items() {
  //Set the return array.
  $result = array();

  //Set up the query. We are retrieving everything so we don't need to use
  //placeholder tokens.
  //At present I don't think I'll need the element data for the objects, that
  //will be needed in the actual archiving hook.

  //The query is dynamic to get the pager to work.
  $res = db_select('cpdcustom', 'cpdc')
    ->fields('cpdc', array('cce_id', 'uid', 'name', 'e_date', 'value'))
    ->orderBy('cpdc.cce_id')
    ->execute();

  //Loop over the query result, generating the key and object to be returned.
  foreach ($res as $res_obj) {
    //The key for the associative array is this cpd item's type concactenated
    //with the entity id.
    $key = 'custom_' . $res_obj->cce_id;

    //Generate the output object. We do this by first creating an array
    //and then casting it to an object. All of the keys in the outarr
    //variable are the object properties expected by cpdarchive.
    $outarr = array();
    $outarr['item_id'] = $res_obj->cce_id;
    $outarr['type'] = 'custom';
    $outarr['name'] = $res_obj->name;
    $outarr['e_date'] = $res_obj->e_date;
    $outarr['value'] = $res_obj->value;

    //Create the array of the extra item data,
    //Let entity_create and entity_save handle the serialisation of
    //the item_data array, otherwise entity_save will serialise the
    //the data again.
    $item_data = array();
    $item_data['uid'] = $res_obj->uid;
    $outarr['item_data'] = $item_data;

    $outobj = (object) $outarr;

    //Add the new output object to the output array.
    $result[$key] = $outobj;
  }

  return $result;
}

/**
 * Implements hook_cpdarch_grab_archiving_data
 * Given an item id and type of custom loads the full item data for that id.
 * @param $type
 *  The type of item being requested.
 * @param $id
 *  The id of the item being requested.
 * @return
 *  Returns a data array formatted in the form used by cpdarchive.
 */
function cpdcustom_cpdarch_grab_archiving_data($type, $id) {
  //Initialise the data array.
  $data = array();

  //If the type of the item is 'custom' then retrive the full item information. Otherwise
  //just return the empty array.

  if($type == 'custom') {

    //Load the CPD custom item entity.
    $item_arr = entity_load('cpdcustom', array($id));

    //If the entity does not exist in the database just return an empty array.
    if(empty($item_arr)) {
      return $data;
    }

    //Extract the lone entity from the array item_arr.
    $item = $item_arr[$id];

    //Extract all of the needed variables from the entity.
    $title = $item->name;
    $value = $item->value;

    //Grab all of the cpds and elements attached to this item.
    //Copied from cpd_node.
    $all_cpds_wsets = $item->elements;

    //Unserialise the elements array if it is serialised.
    if(_cpdcustom_is_serial($all_cpds_wsets)) {
      $all_cpds_wsets = unserialize($item->elements);
    }

    //Add the metadata to the data array.
    $data['orig_id'] = $id;
    $data['name'] = $title;
    $data['type'] = $type;
    $data['e_date'] = $item->e_date;
    $data['value'] = $value;

    //Set the time of archiving to now (well, close enough to now).
    $data['a_date'] = time();

    //Create the array of the extra item data, Don't serialise this array,
    //let entity_save do that (if we don't then it will cause problems).
    $item_data = array();
    $item_data['uid'] = $item->uid;
    $data['item_data'] = $item_data;

    //Add the elements array.
    $data['elements'] = array();

    //Oh great a double foreach loop. Do I actually need this?
    foreach ($all_cpds_wsets as $set_id => $all_cpds) {
      foreach ($all_cpds as $cpd_id => $elements) {

        //Get the values of all the attached elements and add them to the
        //output elements array. I know, I know, there is a HUGE key here.

        //We're just copying whatever is in the database. At the time of
        //writing the element stored in both $val and $key is the element
        //id.
        foreach ($elements as $key => $val) {
          $data['elements'][$set_id][$cpd_id][$key] = $val;
        }
      }
    }
  }

  return $data;
}

/**
 * Implements hook_cpdarch_grab_delete_flag
 * Obtains the delete flag for this module. Only runs if the type argument is custom.
 * @return
 * Returns the flag or nothing if the type is not custom.
 */
function cpdcustom_cpdarch_grab_delete_flag($type) {
  if($type == 'custom') {
    return variable_get('cpdcustom_cpdarch_delete_flag', FALSE);
  }
}

/**
 * Implements hook_cpdh_item_delete
 * Not really a cpdarchive hook, more of a general hook (but I'll put it here until I
 * have more modules calling it).
 * @param $type
 *  The type of the item we wish to delete.
 * @param id
 *  The id of the item we wish to delete.
 */
function cpdcustom_cpdh_item_delete($type, $id) {
  //Only run if type is custom.
  if($type == 'custom') {
    entity_delete('cpdcustom', $id);
  }

  return;
}



/***********************************************/
/* Preprocess theming functions. */
/***********************************************/

/**
 * Process variables for cpdcustom_metadata.tpl.php
 *
 * Most themes utilize their own copy of node.tpl.php. The default is located
 * inside "modules/node/node.tpl.php". Look in there for the full list of
 * variables.
 *
 * The $variables array contains the following arguments:
 * - $node
 * - $view_mode
 * - $page
 *
 * @see node.tpl.php
 */
function template_preprocess_cpdcustom_admin_view(&$variables) {
  //Collect the variables.
  $variables['name'] = check_plain($variables['elements']['name']);
  $variables['username'] = check_plain($variables['elements']['username']);
  $variables['e_date'] = format_date($variables['elements']['e_date'], 'medium');
  $variables['description'] = check_plain($variables['elements']['description']);
  $variables['credit'] = check_plain($variables['elements']['credit']);
  $variables['unit'] = check_plain($variables['elements']['unit']);
  $variables['has_cpds'] = check_plain($variables['elements']['has_cpds']);
}

/**
 * Preprocess function for the cpdcustom_user_view theme. This is the
 * same as the admin view theme except that no username is displayed.
*/
function template_preprocess_cpdcustom_user_view(&$variables) {
  //Collect the variables.
  $variables['name'] = check_plain($variables['elements']['name']);
  $variables['e_date'] = format_date($variables['elements']['e_date'], 'medium');
  $variables['description'] = check_plain($variables['elements']['description']);
  $variables['credit'] = check_plain($variables['elements']['credit']);
  $variables['unit'] = check_plain($variables['elements']['unit']);
  $variables['has_cpds'] = check_plain($variables['elements']['has_cpds']);
}
