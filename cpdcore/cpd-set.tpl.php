<?php
/**
 * @file
 * Default theme implementation for CPD custom sets.
 *
 * Available variables:
 * $set_id: The id of the set.
 * $level: The hierachy level of this set.
 * $name: The name of the set.
 * $cpds: An array of individual cpd items complete with elements. To be
 * rendered by separate template files.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="cpd_set <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <h3><?php print $name ?></h3>
  <?php if($level == 4) {
     print render($domains);
     }
  else {
     print render($cpds);
  }
  ?>

</div>
