<?php
/**
 * @file
 * Default theme implementation for CPD custom sets.
 *
 * Available variables:
 * $set_id: The id of the set.
 * $name: The name of the set.
 * $cpds: An array of individual cpd items complete with elements. To be
 * rendered by separate template files.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="cpd_domain <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <h4><?php print $name ?></h4>
  <?php print render($cpds) ?>
</div>
