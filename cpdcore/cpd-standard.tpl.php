<?php
/**
 * @file
 * Default theme implementation for CPD custom standards.
 *
 * Available variables:
 * $cpd_id: The id of the cpd to be rendered.
 * $name: The name of the cpd.
 * $elements: The elements of the cpd. To be rendered by their own template.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="cpd_standard <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <h4><?php print $name ?></h4>
  <?php if($elements) { ?>
    <ul>
    <?php print render($elements); ?>
    </ul>
  <?php } ?>
</div>
