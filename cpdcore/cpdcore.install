<?php

/**
 * @file
 * The install file for the CPD core module. This file sets up the database
 * tables to record CPD standards, sets and metrics.
 */

/*Define the database schemas for the CPD_item and CPD_set tables.*/
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function cpdcore_schema() {
  $schema['cpd_item'] = array(
    'description' => 'The base table for CPD items.',
    'fields' => array(
      'cpd_id' => array(
        'description' => 'The primary identifier for a CPD item.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'set_id' => array(
        'description' => 'The identifier of the set that this CPD belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'domain_id' => array(
        'description' => 'The identifier of the domain that this CPD belongs to. Only used if this set uses domains. When domains are not in use the default domain id is 0.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'name' => array(
        'description' => 'The human-readable name of the CPD item.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('cpd_id'),
  );

  $schema['cpd_set'] = array(
    'description' => 'The base table for CPD sets.',
    'fields' => array(
      'set_id' => array(
        'description' => 'The primary identifier for a CPD set.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The human-readable name of the CPD set.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'metric_id' => array(
        'description' => 'The metric by which CPDs are measured.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'level' => array(
        'description' => 'Indicates how many levels this standard set has in its hierachy.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 3,
      ),
    ),
    'primary key' => array('set_id'),
  );

  $schema['cpd_metric'] = array(
    'description' => 'The base table for CPD metrics.',
    'fields' => array(
      'metric_id' => array(
        'description' => 'The primary identifier for a CPD metric.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The human-readable name of the CPD metric.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => 'The type of metric this is. At present only numeric and scale are supported. Aggregation method is determined by the metric type.',
        'type' => 'varchar',
        'length' => 32,
        'default' => 'numeric',
        'not null' => TRUE,
      ),
      'limit_type' => array(
        'description' => 'The limit type for this metric',
        'type' => 'varchar',
        'length' => 32,
        'default' => 'unlimited',
        'not null' => TRUE,
      ),
      'unit_name' => array(
        'description' => 'The unit suffix for this metric. Used in some page displays.',
        'type' => 'varchar',
        'length' => 16,
        'default' => '',
        'not null' => TRUE,
      ),
      'limit_range' => array(
        'description' => 'The range of values which this metric provides. Can be one of three things; unlimited, a numerical range in square brackets or a list of values numerical or non-numerical,',
        'type' => 'varchar',
        'length' => 255,
        'default' => 'unlimited',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('metric_id'),
  );

  $schema['cpd_element'] = array(
    'description' => 'The table for elements of the CPD item.',
    'fields' => array(
      'element_id' => array(
        'description' => 'The primary identifier for a CPD element.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'cpd_id' => array(
        'description' => 'The identifier of the CPD that this element belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The human-readable name of the CPD element.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('element_id'),
  );

  //Optional domains table. Domains sit above the cpd_item (standard) but below the cpd_set. They
  //are an optional component.
  $schema['cpd_domain'] = array(
    'description' => 'The table for domains of the CPD item.',
    'fields' => array(
      'domain_id' => array(
        'description' => 'The primary identifier for a CPD domain.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'set_id' => array(
        'description' => 'The identifier of the CPD set that this domain belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The human-readable name of the CPD domain.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('domain_id'),
  );

  return $schema;
}

/*Install the cpdcore module by creating the database tables.*/
/*TODO - Add code to create the cpd settings variables in the system table.*/
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function cpdcore_install() {
  // TODO The drupal_(un)install_schema functions are called automatically in D7.
  // drupal_install_schema('cpdcore')
  //Add the default metric of hours.
  $query = "INSERT INTO {cpd_metric} (name, type, limit_range, unit_name) VALUES ('hours', 'numeric', 'unlimited', 'hrs')";
  // TODO Please review the conversion of this statement to the D7 database API syntax.
  /* db_query($query) */
  $result = $id = db_insert('cpd_metric')
  ->fields(array(
    'name' => 'hours',
    'type' => 'numeric',
    'limit_range' => 'unlimited',
    'unit_name' => 'hrs',
  ))
  ->execute();

  //Set the global metric variable for the first time.
  variable_set('cpd_core_global_metric', 1);

  return;

}

/**
 * Uninstall the cpdcore module by removing the database tables and global metric variable.
 */
function cpdcore_uninstall() {
  variable_del('cpd_core_global_metric');
  return;
}


/**
 * Implements hook_update_N
 * This is the update from the Drupal 6 version to the Drupal 7 version. All that's needed
 * for this upgrade is to delete one of the global variables.
 */
function cpdcore_update_7001() {
  variable_del('cpd_core_cpd_mode');
  return t('Deleted unused variable cpd_core_cpd_mode');
}

/**
* Add the cpd_domains table, add the cpd_dom field to the cpd_item table and add the
* levels field to the cpd_set table. By default
* set all existing records to a level of 3.
*/
function cpdcore_update_7002() {
  $dom_id = array(
    'description' => t('Domain id for this cpd standard. When domains are not in use the default domain id is 0.'),
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  );

  $level = array(
    'description' => t('A value indicating how many levels this standard set has in its hierachy.'),
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 3,
  );

  //Add the dom_id to the cpd_item table.
  db_add_field('cpd_item', 'domain_id', $dom_id);

  //Add the levels field to the cpd_set table.
  db_add_field('cpd_set', 'level', $level);

  //Add the new cpd_domains table.
  $schema['cpd_domain'] = array(
    'description' => 'The table for domains of the CPD item.',
    'fields' => array(
      'domain_id' => array(
        'description' => 'The primary identifier for a CPD domain.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'set_id' => array(
        'description' => 'The identifier of the CPD set that this domain belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The human-readable name of the CPD domain.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('domain_id'),
  );

  //Add the table.
  db_create_table('cpd_domain', $schema['cpd_domain']);

  //Set the default levels

}
