<?php
/**
 * @file
 * Default theme implementation for a single CPD element. This is to be
 * used as part of a list.
 *
 * Available variables:
 * e_id: The element id.
 * name: The element name.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<li id="cpd_element_<?php print $e_id ?>" ><?php print $name ?></li>
